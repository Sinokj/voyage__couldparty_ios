//
//  ViewController.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/19.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DyfTool.h"

@interface ViewController : UIViewController<UITextFieldDelegate>

@property(nonatomic,strong)UIButton* superBtn;

@property (nonatomic, copy)void (^popBlock)(void);

-(void)superBtnClick;
@end

