//
//  UIImage+color.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/11/14.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (color)
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
