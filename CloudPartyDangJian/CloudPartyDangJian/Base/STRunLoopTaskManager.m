//
//  STRunLoopTaskManager.m
//  STRunLoopTaskManager
//
//  Created by ZJXN on 2018/5/18.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "STRunLoopTaskManager.h"

@interface STRunLoopTaskManager()
@property (nonatomic, assign) NSInteger maxRunLoopNumbers;
@property (nonatomic, strong) NSMutableArray *currentTasks;
@end


static STRunLoopTaskManager *instance = nil;

@implementation STRunLoopTaskManager


#pragma mark - private
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self _addRunLoopObserver];
        self.maxRunLoopNumbers = 30;
    }
    return self;
}



- (void)_addRunLoopObserver {
    
    static CFRunLoopObserverRef observer;
    
    CFRunLoopObserverContext context = {
        0,
        (__bridge void*)self,
        &CFRetain,
        &CFRelease,
        NULL
    };
    
    CFRunLoopRef runloop = CFRunLoopGetCurrent();
    observer = CFRunLoopObserverCreate(NULL, kCFRunLoopBeforeWaiting, YES, 0, &_callBack, &context);
    
    CFRunLoopAddObserver(runloop, observer, kCFRunLoopCommonModes);
    
    CFRelease(observer);
}

static void _callBack(CFRunLoopObserverRef observer, CFRunLoopActivity activity, void *info) {
    STRunLoopTaskManager *taskManager = (__bridge STRunLoopTaskManager *)(info);
    if (taskManager.currentTasks.count > 0) {
        TaskBlock taskBlock = taskManager.currentTasks.firstObject;
        taskBlock();
        [taskManager.currentTasks removeObjectAtIndex:0];
    }
}

#pragma mark public

+ (STRunLoopTaskManager *)sharedRunLoopTaskManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [STRunLoopTaskManager new];
    });
    return instance;
}

- (STRunLoopTaskManager *(^)(TaskBlock taskBlock))addTask {
    __weak __typeof(&*self)weakself = self;
    return ^(TaskBlock taskBlock) {
        [weakself.currentTasks addObject:taskBlock];
        if (weakself.currentTasks.count > weakself.maxRunLoopNumbers) {
            [weakself.currentTasks removeObjectAtIndex:0];
        }
        return weakself;
    };
}

@end
