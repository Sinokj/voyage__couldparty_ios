//
//  DECommenView.m
//  Electric2
//
//  Created by ailin on 2017/6/21.
//  Copyright © 2017年 北京电小二网络科技有限公司. All rights reserved.
//

#import "DECommenAlert.h"
#import "SnailPopupController.h"
typedef void (^AlertBlock)(NSInteger index);
@interface DECommenAlert ()
@property (nonatomic, strong)UILabel *titleLabel;
@property (nonatomic, strong)UIButton *leftBtn;
@property (nonatomic, strong)UIButton *rightBtn;
@property (nonatomic, strong)UIButton *middleBtn;
@property (nonatomic, copy)AlertBlock alertBlock;
@end

@implementation DECommenAlert

//- (instancetype)initWithFrame:(CGRect)frame{
//    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor whiteColor];
//        [self drawView];
//    }
//    return self;
//}

- (instancetype)initWithFrame:(CGRect)frame type:(NSInteger)type{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        if (type == 1) {
            [self drawDanBtnView];
        }else if(type == 2){
            [self drawView];
        }
    }
    return self;
}


- (void)drawView{
    self.titleLabel = [UILabel labelWithTextColor:UIColorFromRGB(0x303936) font:14 backgroundColor:nil Text:@"温馨提醒"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.titleLabel.sd_layout
    .leftSpaceToView(self,84 * kWScale)
    .rightSpaceToView(self,84 * kWScale)
    .topSpaceToView(self,86 * kWScale)
    .autoHeightRatio(0);
    
    self.leftBtn = [UIButton buttonWithTextColor:AppTintColor font:14 backgroudColor:nil text:@"点错了"];
    [self.leftBtn addTarget:self action:@selector(actionForLeftBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.leftBtn];
    
    self.leftBtn.sd_layout
    .leftSpaceToView(self,42 * kWScale)
    .widthIs(230 * kWScale)
    .bottomSpaceToView(self, 40 * kWScale)
    .heightIs(68 * kWScale);
    self.leftBtn.sd_cornerRadius = @(4);
    self.leftBtn.layer.borderWidth = 1;
    self.leftBtn.layer.borderColor = AppTintColor.CGColor;
    
    self.rightBtn = [UIButton buttonWithTextColor:[UIColor whiteColor] font:14 backgroudColor:AppTintColor text:@"很确定"];
    [self.rightBtn addTarget:self action:@selector(actionForRightBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.rightBtn];
    
    self.rightBtn.sd_layout
    .rightSpaceToView(self,42 * kWScale)
    .widthIs(230 * kWScale)
    .bottomSpaceToView(self, 40 * kWScale)
    .heightIs(68 * kWScale);
    self.rightBtn.sd_cornerRadius = @(4);
}

- (void)drawDanBtnView{
    self.titleLabel = [UILabel labelWithTextColor:UIColorFromRGB(0x303936) font:14 backgroundColor:nil Text:@"温馨提醒"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.titleLabel.sd_layout
    .leftSpaceToView(self,84 * kWScale)
    .rightSpaceToView(self,84 * kWScale)
    .topSpaceToView(self,86 * kWScale)
    .autoHeightRatio(0);
    
    self.middleBtn = [UIButton buttonWithTextColor:AppTintColor font:14 backgroudColor:nil text:@"知道了"];
    [self.middleBtn addTarget:self action:@selector(actionForMiddleBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.middleBtn];
    
    self.middleBtn.sd_layout
    .leftSpaceToView(self,42 * kWScale)
    .rightSpaceToView(self,42 * kWScale)
    .bottomSpaceToView(self, 40 * kWScale)
    .heightIs(68 * kWScale);
    self.middleBtn.sd_cornerRadius = @(4);
    self.middleBtn.layer.borderWidth = 1;
    self.middleBtn.layer.borderColor = AppTintColor.CGColor;
    
}

+ (void)dismiss{
    [self.sl_popupController dismiss];
}

+ (void)showAlertWithContent:(NSString *)content leftBtn:(NSString *)leftTitle rightBtn:(NSString *)rightTitle tapEvent:(void (^)(NSInteger index))event{
    
    DECommenAlert *alert = [[DECommenAlert alloc]initWithFrame:CGRectZero type: 2];
    alert.size = CGSizeMake(600 * kWScale, 340 * kWScale);
    alert.layer.cornerRadius = 10;
    alert.layer.masksToBounds = YES;
    [alert.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
    [alert.rightBtn setTitle:rightTitle forState:UIControlStateNormal];
    alert.titleLabel.text = content;
    alert.alertBlock = event;
    
    self.sl_popupController = [SnailPopupController new];
    self.sl_popupController.maskTouched = ^(SnailPopupController *popupController){
        [self.sl_popupController dismiss];
    };
    self.sl_popupController.layoutType = PopupLayoutTypeCenter;
    self.sl_popupController.maskType = PopupMaskTypeDefault;
    self.sl_popupController.transitStyle = PopupTransitStyleSlightScale;
    self.sl_popupController.dismissOnMaskTouched = YES;
    [self.sl_popupController presentContentView:alert duration:0.40 elasticAnimated:NO inView:nil];
}

+ (void)showAlertWithContent:(NSString *)content middelBtn:(NSString *)middleBtn tapEvent:(void (^)(NSInteger index))event{
    
    DECommenAlert *alert = [[DECommenAlert alloc]initWithFrame:CGRectZero type: 1];
    alert.size = CGSizeMake(600 * kWScale, 340 * kWScale);
    alert.layer.cornerRadius = 10;
    alert.layer.masksToBounds = YES;
    [alert.middleBtn setTitle:middleBtn forState:UIControlStateNormal];
    alert.titleLabel.text = content;
    alert.alertBlock = event;
    
    self.sl_popupController = [SnailPopupController new];
    self.sl_popupController.maskTouched = ^(SnailPopupController *popupController){
        [self.sl_popupController dismiss];
    };
    self.sl_popupController.layoutType = PopupLayoutTypeCustom;
    self.sl_popupController.maskType = PopupMaskTypeDefault;
    self.sl_popupController.transitStyle = PopupTransitStyleSlightScale;
    self.sl_popupController.dismissOnMaskTouched = YES;
    [self.sl_popupController presentContentView:alert duration:0.50 elasticAnimated:YES inView:nil];
}

- (void)actionForLeftBtn{
    if (self.alertBlock) {
        self.alertBlock(0);
    }
}

- (void)actionForRightBtn{
    if (self.alertBlock) {
        self.alertBlock(1);
    }
}

- (void)actionForMiddleBtn{
    if (self.alertBlock) {
        self.alertBlock(0);
    }
}


@end
