//
//  UIView+Extension.h
//  JYB
//
//  Created by hao on 16/8/26.
//  Copyright © 2016年 sanmiao. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - view
@interface UIView(LKExtension)
/**
 *  当前View所属的控制器
 */
@property(nonatomic, weak, readonly) UIViewController *curVc;

@end

//******************************************************
#pragma mark - UIView
@interface UIView(Extension)
+ (UIView *)viewWithBackgroundColor:(UIColor *)color;
@end

#pragma mark - label
@interface UILabel(Extension)
/**
 *  创建一个label
 */
+ (UILabel *)labelWithTextColor:(UIColor *)textColor font:(CGFloat)textFont backgroundColor:(UIColor *)backColor Text:(NSString *)text ;

/**
 *  改变行间距
 */
+ (void)changeLineSpaceForLabel:(UILabel *)label WithSpace:(float)space;

/**
 *  改变字间距
 */
+ (void)changeWordSpaceForLabel:(UILabel *)label WithSpace:(float)space;

/**
 *  改变行间距和字间距
 */
+ (void)changeSpaceForLabel:(UILabel *)label withLineSpace:(float)lineSpace WordSpace:(float)wordSpace;

@end

//******************************************************
#pragma mark - UIImageView
@interface UIImageView(Extension)
+ (UIImageView *)alImageViewWithImageName:(NSString *)imageName;
@end

//*********************************************************
#pragma mark - button

typedef NS_ENUM(NSInteger, ImagePosition) {
    ImagePositionLeft = 0,  // 图片在左，文字在右，默认
    ImagePositionRight,     // 图片在右，文字在左
    ImagePositionTop,       // 图片在上，文字在下
    ImagePositionBottom,    // 图片在下，文字在上
};

@interface UIButton(Extension)

/**
 *  创建一个button(不带图片)
 */
+ (UIButton *)buttonWithTextColor:(UIColor *)textColor font:(CGFloat)textFont backgroudColor:(UIColor *)backColor text:(NSString *)text;

/**
 *  创建一个button(带图片)
 */
+ (UIButton *)buttonWithTextColor:(UIColor *)textColor font:(CGFloat)textFont backgroudColor:(UIColor *)backColor image:(UIImage *)image text:(NSString *)text;

/**
 *  创建一个button(只是图片)
 */
+ (UIButton *)buttonWithImageName:(NSString *)imageName;

/**
 *  设置图片的位置
 */
- (void)setImagePosition:(ImagePosition)postion spacing:(CGFloat)spacing;


@end


