//
//  VersionModel.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/19.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VersionModel : NSObject
@property (nonatomic , copy) NSString              * vcContent;
@property (nonatomic , assign) NSInteger              bUse;
@property (nonatomic , assign) NSInteger              nid;
@property (nonatomic , copy) NSString              * vcVersion;
@property (nonatomic , copy) NSString              * dtReg;
@end

NS_ASSUME_NONNULL_END
