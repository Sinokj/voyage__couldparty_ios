//
//  STUtils.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "STUtils.h"
#import <Masonry.h>
#import <TZImagePickerController.h>
@implementation STUtils
+ (void)imagePickerWithVc:(UIViewController *)vc
           maxImagesCount:(NSInteger)imageCount
       finishPickingBlock:(void(^)(NSArray<UIImage *> *photos))finishBlock{
    TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:imageCount delegate:nil];
    imagePicker.naviBgColor = AppTintColor;
    imagePicker.allowPickingVideo = NO;
    imagePicker.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (finishBlock) {
            finishBlock(photos);
        }
    };
    [vc presentViewController:imagePicker animated:YES completion:nil];
}

+ (void)setupView:(UIView *)view
    cornerRadius:(CGFloat)radius
          bgColor:(UIColor *)bgColor
          borderW:(CGFloat)borderW
      borderColor:(UIColor *)borderColor {
    view.layer.cornerRadius = radius;
    view.backgroundColor = bgColor;
    view.layer.borderWidth = borderW;
    view.layer.borderColor = borderColor.CGColor;
    view.layer.masksToBounds = YES;
}

+ (id)objectForKey: (NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}
+ (void)setObject:(id)object
           forKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (BOOL)isLogin {
    return [[STUtils objectForKey:KIsLogin] boolValue];
}


//cookie管理
+ (void)saveCookieWithUrl:(NSString *)url{
    
    NSArray *dongyiCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:url]];
    
    NSMutableArray *cookies = [NSMutableArray array];
    [cookies addObjectsFromArray:dongyiCookies];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:cookies];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:@"cookie"];
}

+ (void)setupCookie{
    
    NSData *cookiesdata = [[NSUserDefaults standardUserDefaults] objectForKey:@"cookie"];
    if([cookiesdata length]) {
        NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookiesdata];
        NSHTTPCookie *cookie;
        for (cookie in cookies) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
    }
}

+ (void)clearCookie{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"" forKey:@"cookie"];
}
@end
