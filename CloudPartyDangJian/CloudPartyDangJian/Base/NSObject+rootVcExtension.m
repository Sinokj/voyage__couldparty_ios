//
//  NSObject+rootVcExtension.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/18.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "NSObject+rootVcExtension.h"
#import "AppDelegate.h"

@implementation NSObject (rootVcExtension)
- (TabbarController *)tabBarVc {
    
    return [(AppDelegate *)[UIApplication sharedApplication].delegate tabBarVc];
}

- (HomeController *)homeVc {
    UINavigationController *nav = self.tabBarVc.viewControllers[0];
    return nav.viewControllers[0];
}

- (BulletinController *)bullVc {
    UINavigationController *nav = self.tabBarVc.viewControllers[1];
    return nav.viewControllers[0];
}

- (STMineViewController *)mineVc {
    UINavigationController *nav = self.tabBarVc.viewControllers[3];
    return nav.viewControllers[0];
}

- (WebViewController *)webVc {
    UINavigationController *nav = self.tabBarVc.viewControllers[2];
    return nav.viewControllers[0];
}

@end
