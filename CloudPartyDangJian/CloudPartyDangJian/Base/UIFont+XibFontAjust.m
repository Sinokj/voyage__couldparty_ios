//
//  UIFont+XibFontAjust.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/22.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "UIFont+XibFontAjust.h"
#import <objc/runtime.h>
@implementation UIFont (XibFontAjust)

@end

@implementation UILabel (XibFontAjust)

+ (void)load {
    Method sysInit = class_getInstanceMethod([self class], @selector(initWithCoder:));
    Method myInit = class_getInstanceMethod([self class], @selector(st_initWithCoder:));
    method_exchangeImplementations(sysInit, myInit);
}
- (instancetype)st_initWithCoder:(NSCoder *)coder
{
    [self st_initWithCoder:coder];
    self.font = FontSystem(MYDIMESCALEW(self.font.pointSize));
    return self;
}

@end

@implementation UITextField (XibFontAjust)

+ (void)load {
    Method sysInit = class_getInstanceMethod([self class], @selector(initWithCoder:));
    Method myInit = class_getInstanceMethod([self class], @selector(st_initWithCoder:));
    method_exchangeImplementations(sysInit, myInit);
}
- (instancetype)st_initWithCoder:(NSCoder *)coder
{
    [self st_initWithCoder:coder];
    self.font = FontSystem(MYDIMESCALEW(self.font.pointSize));
    return self;
}
@end

@implementation UITextView (XibFontAjust)

+ (void)load {
    Method sysInit = class_getInstanceMethod([self class], @selector(initWithCoder:));
    Method myInit = class_getInstanceMethod([self class], @selector(st_initWithCoder:));
    method_exchangeImplementations(sysInit, myInit);
}
- (instancetype)st_initWithCoder:(NSCoder *)coder
{
    [self st_initWithCoder:coder];
    self.font = FontSystem(MYDIMESCALEW(self.font.pointSize));
    return self;
}
@end

