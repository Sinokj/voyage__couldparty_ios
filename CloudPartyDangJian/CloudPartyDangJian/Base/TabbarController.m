//
//  TabbarController.m
//  SinoMT
//
//  Created by df on 16/6/13.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "TabbarController.h"
#import "HomeController.h"
#import "AboutUsController.h"
#import "BulletinController.h"
#import "WebViewController.h"
//#import "MineController.h"
//新的个人中心页面
#import "STMineViewController.h"
#import "STNavigationController.h"
@interface TabbarController ()<UITabBarControllerDelegate>

@end

@implementation TabbarController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tabBar.tintColor = AppTintColor;
    self.delegate = self;
    
    self.tabBar.backgroundColor = [UIColor whiteColor];
    [self setUpViewController:[[HomeController alloc] init] title:@"首页" imageName:@"icon_homepage_nor" selectedImage: @"icon_homepage_pre"];
    [self setUpViewController:[[BulletinController alloc] init] title:@"要闻" imageName:@"icon_notice_nor" selectedImage: @"icon_notice_pre"];
    
    WebViewController *about = [WebViewController new];
    about.type = @"1";
    about.str = [NSString stringWithFormat:@"%@/%@%@",BaseUrl,aboutUs,[[NSUserDefaults standardUserDefaults] valueForKey:@"partyID"]?[[NSUserDefaults standardUserDefaults] valueForKey:@"partyID"]:@2];
    
    [self setUpViewController:about title:@"关于我们" imageName:@"icon_aboutus_nor" selectedImage: @"icon_aboutus_pre"];
   about.tit = @"关于我们";
    [self setUpViewController:[[STMineViewController alloc] init] title:@"我的" imageName:@"icon_mine_nor" selectedImage: @"icon_mine_pre"];
    
}
- (void)setUpViewController:(UIViewController *)tableVC title:(NSString *)title imageName:(NSString *)image selectedImage:(NSString *)selectedImage {
    
    tableVC.tabBarItem.title = title;
    
    tableVC.tabBarItem.image = [UIImage imageNamed:image];
    
    tableVC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

    STNavigationController *navgationC = [[STNavigationController alloc] initWithRootViewController:tableVC];
    
    navgationC.view.backgroundColor = [UIColor whiteColor];
    
    [navgationC.navigationBar setBackgroundImage:[UIImage imageWithColor: AppTintColor] forBarMetrics:(UIBarMetricsDefault)];
    
    navgationC.navigationBar.translucent = NO;

    [self addChildViewController:navgationC];
    
}
//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
//    if (viewController == self.viewControllers[2]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"敬请期待" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//        [alert show];
//      [self performSelector:@selector(dimissAlert:)withObject:alert afterDelay:2.0];
//       return NO;
//
//    }else{
//        return YES;
//    }
//
//}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
    
    if (viewController.childViewControllers.count > 0 && [viewController.childViewControllers[0] isKindOfClass:[WebViewController class]]) {
        WebViewController *abount = (WebViewController *) viewController.childViewControllers[0];
        abount.type = @"1";
        abount.str = [NSString stringWithFormat:@"%@/%@%@",BaseUrl,aboutUs,[STUtils objectForKey:KDangJianType]? [STUtils objectForKey:KDangJianType]: @2];
        
        [abount reloadWithUrl:abount.str];
        
        
    }
}
- (void) dimissAlert:(UIAlertView *)alert {
    if(alert){
        [alert dismissWithClickedButtonIndex:[alert cancelButtonIndex] animated:YES];
    }
}
@end
