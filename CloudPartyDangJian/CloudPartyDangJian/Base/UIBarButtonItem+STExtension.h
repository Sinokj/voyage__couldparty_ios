//
//  UIBarButtonItem+STExtension.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (STExtension)
+ (instancetype)barButtonItemWithImageName:(NSString *)imageName block:(void(^)(id sender))block;
+ (instancetype)barButtonItemWithTitle:(NSString *)title block:(void (^)(id sender))block;
+ (instancetype)barButtonItemWithImageName:(NSString *)imageName title:(NSString *)title block:(void(^)(id sender))block;
@end
