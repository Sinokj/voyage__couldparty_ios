//
//  MBProgressHUD+DyfAdd.h
//  SinoCommunity
//
//  Created by df on 2017/2/14.
//  Copyright © 2017年 df. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (DyfAdd)
/**
 *  提示文字不会自动消失
 *
 *  @param message 要显示的文字
 *  @param view    要添加的View
 */
+ (void)showMessage:(NSString *)message ToView:(UIView *)view userInteractionEnabled:(BOOL)enable;
/**
 *  提示文字设置消失时间
 *
 *  @param message 要显示的文字
 *  @param time    显示的时间
 *  @param view    要添加的View
 */
+ (void)showMessage:(NSString *)message RemainTime:(CGFloat)time ToView:(UIView *)view userInteractionEnabled:(BOOL)enable;
/**
 *  提示菊花状
 *
 *  @param message 要显示的文字
 *  @param time    显示的时间
 *  @param view    要添加的View
 */
+ (void)showProgressMessage:(NSString *)message RemainTime:(CGFloat)time ToView:(UIView *)view userInteractionEnabled:(BOOL)enable;
/**
 *  隐藏提示 添加在window上的有效
 */
+ (void)hideHUD;

@end
