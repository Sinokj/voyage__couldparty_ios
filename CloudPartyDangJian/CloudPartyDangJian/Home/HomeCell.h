//
//  HomeCell.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeModel.h"
#import "CollectViewModel.h"

@interface HomeCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *img;

@property (nonatomic, assign) NSInteger showNumber;
@property (nonatomic, weak, readonly) UILabel *titleLab;


//- (void)setModel:(HomeModel *)homeM andTitle:(NSString *)title;

- (void)setModel:(CollectViewModel *)collectModel andTitle:(NSString *)title;


@end

