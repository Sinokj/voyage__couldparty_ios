//
//  PublicityModel.m
//  EPartyConstruction
//
//  Created by df on 2017/6/22.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "PublicityModel.h"

@implementation PublicityModel

+ (NSDictionary *)objectClassInArray{
    return @{@"objects" : [Publicity class]};
}

@end

@implementation Publicity

@end
