//
//  PartyCommitteeModel.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "PartyCommitteeModel.h"

@implementation PartyCommitteeModel

+ (PartyCommitteeModel *)createModelWithDic:(NSDictionary *)dic {
    PartyCommitteeModel *model = [[PartyCommitteeModel alloc] init];
    [model setValuesForKeysWithDictionary:dic];
    return model;
}
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {}

@end
