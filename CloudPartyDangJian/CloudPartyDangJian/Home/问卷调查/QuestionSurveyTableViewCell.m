//
//  QuestionSurveyTableViewCell.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/5.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "QuestionSurveyTableViewCell.h"
#import <Masonry.h>
#define LMargin 8
#define TMargin 3
#define BMargin 6
@interface QuestionSurveyTableViewCell()
@property(nonatomic,weak)UILabel *namelabel;
@property(nonatomic,weak)UIImageView *iconImageView;
@end
@implementation QuestionSurveyTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 添加子控件
        [self prepareLayout];
    }
    return self;
}

-(void)prepareLayout{
    //self.backgroundColor = [UIColor redColor];
    // 1.头像
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = [UIImage imageNamed:@"ic_doc"];
    [self.contentView addSubview:iconView];
    iconView.contentMode =  UIViewContentModeScaleAspectFit;
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(5);
//        make.top.offset(8);
        make.width.offset(50);
        make.height.offset(55);
        make.centerY.offset(0);
       // make.height.lessThanOrEqualTo(@100);
    }];
    // 2.昵称
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.text = @"党建工作调查问卷";
    [self.contentView addSubview:nameLabel];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_right).offset(3);
        //make.width.offset(50);
        //make.top.offset(10);
        make.centerY.equalTo(iconView.mas_centerY);
    }];
    self.iconImageView = iconView;
    self.namelabel = nameLabel;
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        // 让contentView距离父控件"cell"四边0间距
        make.edges.equalTo(self).mas_offset(UIEdgeInsetsZero);
        //make.bottom.equalTo(iconView.mas_bottom).offset(10);
    }];
}
-(void)setQustionsurvey:(QuestionSurvey *)qustionsurvey{
    if (_qustionsurvey != qustionsurvey) {
        _qustionsurvey = nil;
        _qustionsurvey = qustionsurvey;
        self.namelabel.text = qustionsurvey.vcTitle;
    }
}

@end
