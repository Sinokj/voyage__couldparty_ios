//
//  QuestionsSurveyViewController.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/5.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "QuestionSurveyModel.h"
@interface QuestionsSurveyViewController : ViewController
@property(nonatomic,strong)QuestionSurveyModel *questionsurveyModel;
@property(nonatomic,strong)QuestionSurvey *WebQuestionsurvey;
@end
