//
//  ExaminationOnlineModel.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/6.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ExaminationOnlin;
@interface ExaminationOnlineModel : NSObject
@property(nonatomic,strong)NSArray<ExaminationOnlin
*> *objects;
@end
@interface ExaminationOnlin : NSObject
@property(nonatomic,copy)NSString *vcTitle;
@property(nonatomic,assign)NSInteger examedNumber;
@property(nonatomic,assign)NSInteger isBegin;
@property(nonatomic,assign)NSInteger nTopicsTotal;
@property(nonatomic,assign)NSInteger answerdNumber;
@end
