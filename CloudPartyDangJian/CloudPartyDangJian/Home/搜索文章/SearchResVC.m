//
//  SearchResVC.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/10/30.
//  Copyright © 2017年 Dyf. All rights reserved.
//


#import "SearchResVC.h"
#import "ListCell.h"
#import "WebViewController.h"


@interface SearchResVC ()<UITableViewDelegate,UITableViewDataSource,WebViewDelegate>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) NSMutableArray<NewList *> *sourceArr;

@property (nonatomic, strong) NSIndexPath *indexp;

@end

@implementation SearchResVC
- (NSMutableArray<NewList *> *)sourceArr {
    if (!_sourceArr) {
        _sourceArr = [NSMutableArray array];
    }
    return _sourceArr;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blueColor]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.sourceArr = [NSMutableArray array];
    
    self.title = @"搜索结果";
    
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
    @weakify(self);
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithPage:1 text:self.text];
        self.page = 1;
    }];
    self.tableV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        self.page +=1;
        [self requestDataWithPage:self.page text:self.text];
    }];
    self.page = 1;
    
    
//    [self requestDataWithPage:1 text:self.text];
    [self.sourceArr addObjectsFromArray:self.listM.objects];
    [self.tableV.mj_header beginRefreshing];
}

- (void)requestDataWithPage:(NSInteger)pageNo text:(NSString *)text {
    
    [DataRequest GetWithURL:[NSString stringWithFormat:@"%@nCommitteeId=%@&nPageNo=%zd&nPageSize=15&vcTitle=%@",searchNews,[STUtils objectForKey:KDangJianType]?[STUtils objectForKey:KDangJianType]:@2, pageNo, text] :^(id reponserObject) {
        
        self.listM = [ListModel mj_objectWithKeyValues:reponserObject];
        
        if (self.listM.objects.count == 0 || self.listM.objects == nil) {
            
            [MBProgressHUD showMessage:@"暂无数据" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        }
        if (pageNo > 1) {
            [self.sourceArr addObjectsFromArray: [NewList mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]]];
        }else {
            [self.sourceArr removeAllObjects];
            [self.sourceArr addObjectsFromArray:[NewList mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]]];
        }
        if (reponserObject[@"objects"] == nil || [reponserObject[@"objects"] count] == 0) {
            [MBProgressHUD showMessage:@"暂无数据" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        }
        
        [self.tableV reloadData];
        [self.tableV.mj_header endRefreshing];
        [self.tableV.mj_footer endRefreshing];
    }];
    
}

- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableV registerNib:[UINib nibWithNibName:@"ListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.model = self.sourceArr[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
    
    WebViewController *webVC = [WebViewController new];
    
    webVC.type = @"1";
    webVC.str = self.sourceArr[indexPath.row].url;
    
    NewList *neList = self.sourceArr[indexPath.row];
    
    
    webVC.canShare = neList.nShared;
    
    webVC.tit = neList.vcType;
    
    webVC.shareImage = neList.vcPath;
    webVC.shareTitle = neList.vcTitle;
    webVC.shareDescribe = neList.vcDescribe;
    
    webVC.articleId = neList.nId;
    webVC.vcType = neList.vcType;
    
    
    webVC.delegate = self;
    self.indexp = indexPath;
    
    [self.navigationController pushViewController:webVC animated:YES];
    
}

- (void)getNewState:(NSInteger)state {
    
    self.sourceArr[self.indexp.row].nStatus = state;
    
    [self.tableV reloadRowsAtIndexPaths:@[self.indexp] withRowAnimation:(UITableViewRowAnimationNone)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 108;
}

@end

