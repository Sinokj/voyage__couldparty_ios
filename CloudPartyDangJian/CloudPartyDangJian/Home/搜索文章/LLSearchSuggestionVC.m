//
//  LLSearchSuggestionVC.m
//  LLSearchView
//
//  Created by 王龙龙 on 2017/7/25.
//  Copyright © 2017年 王龙龙. All rights reserved.
//

#import "LLSearchSuggestionVC.h"

#import "LLSearchViewConst.h"

#import "DataRequest.h"

#import "HeaderUrl.h"

#import "ListModel.h"

#import <MJExtension/MJExtension.h>

@interface LLSearchSuggestionVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *contentView;
@property (nonatomic, copy) NSString *searchTest;

@property (nonatomic, strong) ListModel *model;

@end

@implementation LLSearchSuggestionVC

- (void)setModel:(ListModel *)model {
    _model = model;
    
    [_contentView reloadData];
    
}

- (UITableView *)contentView
{
    if (!_contentView) {
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        
        self.contentView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height) style:UITableViewStylePlain];
        _contentView.delegate = self;
        _contentView.dataSource = self;
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.tableFooterView = [UIView new];
    }
    return _contentView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.contentView];
}

- (void)searchTestChangeWithTest:(NSString *)test
{
    _searchTest = test;
    
    [self requestResListWithText:test];
}

#pragma mark - AboutNetworking

- (void)requestResListWithText:(NSString *)text {
    
    
    [DataRequest CatlikeGetWithURL:[NSString stringWithFormat:@"%@vcPlatform=e党建&nPageNo=1&nPageSize=10&vcTitle=%@", searchNews,text] :^(id reponserObject) {
        
       self.model = [ListModel mj_objectWithKeyValues:reponserObject];
        
    }];
    
}

#pragma mark - UITableViewDataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _model.objects ? _model.objects.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"CellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = _model.objects[indexPath.row].vcTitle;
    
    return cell;
}


#pragma mark - UITableViewDelegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchBlock) {
        self.searchBlock(_model.objects[indexPath.row]);
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
