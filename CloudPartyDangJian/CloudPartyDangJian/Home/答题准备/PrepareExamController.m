//
//  PrepareExamController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "PrepareExamController.h"
#import "ExamsModel.h"
#import "ExamController.h"

@interface PrepareExamController ()

@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@property (nonatomic, strong) ExamsModel *examsM;

@property (weak, nonatomic) IBOutlet UILabel *titleL;

@property (weak, nonatomic) IBOutlet UILabel *haveDoneL;

@property (weak, nonatomic) IBOutlet UILabel *numberL;

@property (weak, nonatomic) IBOutlet UILabel *totalL;

@property (weak, nonatomic) IBOutlet UILabel *passL;

@property (weak, nonatomic) IBOutlet UILabel *adminL;

@property (weak, nonatomic) IBOutlet UILabel *dateL;

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (weak, nonatomic) IBOutlet UILabel *dateLab;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBgHeightConstraint;

@end

@implementation PrepareExamController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
    //self.title = @"准备答题";
    if (self.title == nil) {
        self.title = @"掌上答题";
    }
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestData];

}

- (void)requestData {
    

    
    [DataRequest PostWithURL:ExamsQuestions parameter:@{@"nTopicsId": self.isMine == YES ? self.nId : @(self.examList.nId)} :^(id reponserObject) {
        
        self.examsM = [ExamsModel mj_objectWithKeyValues:reponserObject];
        
        NSString *btnText =  self.examsM.isBegin == 0 ? @"开始答题" :(self.examsM.isBegin == 1 ? @"继续答题" : @"查看结果");
        
        [self.sureBtn setTitle:btnText forState:(UIControlStateNormal)];

    }];
    
}

- (void)prepareLayout {
    self.topBgHeightConstraint.constant = MYDIMESCALEH(110);
    
//    self.stackView.layer.cornerRadius = 5;
    self.contentView.layer.cornerRadius = 8;
    self.contentView.layer.shadowRadius = 3;
    self.contentView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.contentView.layer.masksToBounds = true;
    
    if (self.examList.vcTitle.length >= 9) {
        
        _titleL.text = [self.examList.vcTitle substringWithRange:NSMakeRange(8, self.examList.vcTitle.length - 8)];
        
        _dateLab.text = [[self.examList.vcTitle substringWithRange:NSMakeRange(0, 8)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
    }else {
        _dateLab.text = @"";
        _titleL.text = self.examList.vcTitle;
    }
    
     _titleL.textAlignment = NSTextAlignmentCenter;
    
    NSMutableAttributedString *attHaveDown = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld/%ld",self.examList.answerdNumber,self.examList.nTopicsTotal]];
    [attHaveDown addAttribute:NSForegroundColorAttributeName value:RGB(255, 195, 72) range:NSMakeRange(0, [@(self.examList.answerdNumber).description length])];
    self.haveDoneL.attributedText = attHaveDown;
    
    self.numberL.text = [NSString stringWithFormat:@"%ld",self.examList.examedNumber];
    
    self.totalL.text = [NSString stringWithFormat:@"%ld",self.examList.nTotal];
    
    
    self.passL.text = [NSString stringWithFormat:@"%ld",self.examList.nPass];
    
    
    self.adminL.text = self.examList.vcRegister;
    
    self.dateL.text = [self.examList.dtReg substringToIndex:self.examList.dtReg.length - 9];
    
    

    NSString *str = self.examList.isBegin == 0 ? @"开始答题" :(self.examList.isBegin == 1 ? @"继续答题" : @"查看结果");
    
    
    [self.sureBtn setTitle:str forState:(UIControlStateNormal)];
    [STUtils setupView:self.sureBtn cornerRadius:MYDIMESCALEW(18) bgColor:AppTintColor borderW:0 borderColor:nil];
    
    [self.sureBtn addTarget:self action:@selector(btnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
//    [self canClickWithStr:str];
    

}

//- (void)canClickWithStr:(NSString *)str {
//    if ([str isEqualToString:@"答 题 已 完 成"]) {
//        self.sureBtn.enabled = YES;
//    }else {
//        self.sureBtn.enabled = YES;
//    }
//    
//}

- (void)btnClick:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"开始答题"]) {
        
        [DataRequest PostWithURL:BeginExam parameter:@{@"nTopicsId": @(self.examList.nId)} :^(id reponserObject) {
            
            ExamController *examC = [ExamController new];
            
            examC.examList = self.examList;
            
            examC.examsM = self.examsM;
            
            [self.navigationController pushViewController:examC animated:YES];
        }];
        
    }else if ([sender.currentTitle isEqualToString:@"继续答题"]) {
        [DataRequest PostWithURL:ContinueExam parameter:@{@"nTopicsId": @(self.examList.nId)} :^(id reponserObject) {
            DLog(@"--------继续答题--------->>>  %@",reponserObject);
            
            ExamController *examC = [ExamController new];
            
            examC.examsM = self.examsM;
            
            examC.examList = self.examList;
            
            examC.answerArr = [NSMutableArray arrayWithArray:reponserObject[@"objects"]];
            
            [self.navigationController pushViewController:examC animated:YES];
        }];


    }else if ([sender.currentTitle isEqualToString:@"查看结果"]) {
        
        [DataRequest PostWithURL:ContinueExam parameter:@{@"nTopicsId": @(self.examList.nId)} :^(id reponserObject) {
            DLog(@"----------------->>>  %@",reponserObject);
            
            ExamController *examC = [ExamController new];
            
            examC.examsM = self.examsM;
            
            examC.examList = self.examList;
            
            examC.isOver = YES;
            
            examC.answerArr = [NSMutableArray arrayWithArray:reponserObject[@"objects"]];
            
            [self.navigationController pushViewController:examC animated:YES];
        }];

    }
}



@end
