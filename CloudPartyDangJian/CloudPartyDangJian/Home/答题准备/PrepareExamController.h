//
//  PrepareExamController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "ExamListModel.h"


/**
 查看结果
 */
@interface PrepareExamController : ViewController

@property (nonatomic, strong) ExamLists *examList;

@property (nonatomic, strong) NSString *nId;

@property (nonatomic, assign) BOOL isMine;


@end
