//
//  SuggestSendController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/26.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "SuggestSendController.h"

@interface SuggestSendController ()

@property (nonatomic, strong) UITextView *textView;

@end

@implementation SuggestSendController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"意见征集";
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.9463 green:0.1012 blue:0.1432 alpha:1.0];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self prepareLayout];

}

- (void)prepareLayout {
   
    UIImageView *headImageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 80*ScreenWidth/320)];
    
    headImageV.image = [UIImage imageNamed:@"gold_color_bg"];
    
    [self.view addSubview:headImageV];
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, ScreenWidth - 60, 40)];
    
    headLabel.font = FontSystem(14);
    
    headLabel.text = @"请把您的意见或者建议描述清楚,我们会尽快解决";
    
    headLabel.numberOfLines = 0;
    
    headLabel.textAlignment = NSTextAlignmentCenter;
    
    headLabel.centerX = headImageV.centerX;
    
    headLabel.centerY = headImageV.centerY;
    
    [headImageV addSubview:headLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, headImageV.bottom + 5, ScreenWidth - 40, 20)];
    
    titleLabel.font = FontSystem(15);
    
    titleLabel.text = self.suggest.vcTopic;
    
    [self.view addSubview:titleLabel];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(20, titleLabel.bottom + 5, titleLabel.width, 100)];
    
    [self.view addSubview:self.textView];
    
    self.textView.layer.borderWidth = 1;
    
    self.textView.layer.cornerRadius = 3;
    
    UIButton *sendBtn = [UIButton buttonWithType:(UIButtonTypeSystem)];
    
    [self.view addSubview:sendBtn];
    
    sendBtn.frame = CGRectMake(30, self.textView.bottom + 30, ScreenWidth - 60, 40);
    
    [sendBtn setTitle:@"提  交" forState:(UIControlStateNormal)];
    
    [sendBtn setTintColor:[UIColor whiteColor]];
    
    [sendBtn setBackgroundColor: [UIColor colorWithRed:0.9463 green:0.1012 blue:0.1432 alpha:1.0]];
    
    sendBtn.layer.cornerRadius = 3;
    
    [sendBtn addTarget:self action:@selector(sendSuggest) forControlEvents:(UIControlEventTouchUpInside)];
    
}

- (void)sendSuggest {
    if (self.textView.text == nil) {
        
        [MBProgressHUD showMessage:@"请填写内容" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        
        return;
    }
    [DataRequest PostWithURL:SubmitSuggest parameter:@{@"nTopicsId" : @(self.suggest.nId), @"vcContent":self.textView.text} :^(id reponserObject) {
        
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            [MBProgressHUD showMessage:@"提交成功" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });

            
            }else {
                [MBProgressHUD showMessage:@"提交失败" RemainTime:2 ToView:self.view userInteractionEnabled:YES];

            }

    }];
}

@end
