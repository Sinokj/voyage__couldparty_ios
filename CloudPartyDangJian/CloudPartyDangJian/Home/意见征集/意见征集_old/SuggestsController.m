//
//  SuggestController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/10/8.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "SuggestsController.h"
#import "VoteCell.h"
#import "WebViewController.h"

@interface SuggestsController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@end

@implementation SuggestsController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"意见征集";
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];


}

- (void)requestData {
    
}

- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableV registerClass:[VoteCell class] forCellReuseIdentifier:@"VoteCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.suggestsM.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VoteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VoteCell" forIndexPath:indexPath];
    cell.title = self.suggestsM.objects[indexPath.row].vcTitle;
    cell.endTime = self.suggestsM.objects[indexPath.row].dtEnd;
    cell.isVoted = self.suggestsM.objects[indexPath.row].nJoined;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
//    getTopics_Contentv.do?nId=1
//    if (self.suggestsM.objects[indexPath.row].nJoined == 0) {
    
        WebViewController *web = [WebViewController new];
        web.type = @"1";
        web.tit = @"意见征集";
        web.str = [NSString stringWithFormat:@"%@/getTopics_Content.do?nId=%ld",HostUrl,self.suggestsM.objects[indexPath.row].nId];
        
        [self.navigationController pushViewController:web animated:YES];
        
//    }else {
//        [MBHelper addHUDInView:self.view text:@"您已参加过" hideAfterDelay:2];
//    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}


@end
