//
//  SuggestController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/10/8.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "SuggestsM.h"

@interface SuggestsController : ViewController

@property (nonatomic, strong) SuggestsM *suggestsM;

@end
