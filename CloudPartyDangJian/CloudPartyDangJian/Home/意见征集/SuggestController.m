//
//  SuggestController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "SuggestController.h"
#import "SuggestCell.h"
#import "SuggestModel.h"
#import "SuggestShowController.h"

@interface SuggestController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) SuggestModel *suggestM;

@end

@implementation SuggestController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.title = @"意见征集";
//    self.navigationController.navigationBar.backgroundColor = RGB(230, 230, 230);
    
    self.view.backgroundColor = RGB(230, 230, 230);
//    [self setupLeftItem];
    
    [self prepareLayout];

}
- (void)setupLeftItem {
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:ImageName(@"back") style:UIBarButtonItemStyleDone target:self action:@selector(backOff)];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)backOff {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self requestSuggestData];
    
}

- (void)requestSuggestData {
    DLog(@"%@",[STUtils objectForKey:KDangJianType]);
//    [MBProgressHUD showMessage:@"正在加载..." RemainTime:1.0 ToView:self.view userInteractionEnabled:YES];
    [DataRequest PostWithURL:SuggestList parameter:@{@"nCommitteeId":[STUtils objectForKey:KDangJianType]} :^(id reponserObject) {
        
        self.suggestM = [SuggestModel mj_objectWithKeyValues:reponserObject];
        [self.tableV reloadData];
        
    }];
    
}

-  (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64 - 49) style:(UITableViewStylePlain)];
    self.tableV.backgroundColor = RGB(230, 230, 230);
    [self.view addSubview:self.tableV];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
//    self.tableV.contentInset = UIEdgeInsetsMake(5, 0, 5, 0);
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    @weakify(self);
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestSuggestData];
        [self.tableV.mj_header endRefreshing];
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.suggestM.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"SUGGESTCELL";
    SuggestCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SuggestCell" owner:nil options:nil][0];
    }
    cell.suggest = self.suggestM.objects[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
    
    SuggestShowController *SSC = [[SuggestShowController alloc] init];
    SSC.suggest = self.suggestM.objects[indexPath.row];
    SSC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:SSC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return MYDIMESCALEH(110);
}


@end
