//
//  PublishView.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/4.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublishView : UIView
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

- (void)setupThisView;

@end
