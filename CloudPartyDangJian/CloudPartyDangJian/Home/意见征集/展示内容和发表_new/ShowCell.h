//
//  ShowCell.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/3.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
