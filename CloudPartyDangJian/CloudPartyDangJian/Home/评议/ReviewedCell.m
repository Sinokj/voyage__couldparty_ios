//
//  ReviewedCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ReviewedCell.h"
#import "DyfTool.h"
#import <UIKit/UIKit.h>
#import <Masonry.h>


@interface ReviewedCell ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *titleLabel;

//@property (nonatomic, strong) UITextField *scoreTF;

@property (nonatomic, strong) UILabel *label;

@end
@implementation ReviewedCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.numberLabel = [UILabel customLablWithFrame:CGRectMake(10, 5, 20, 30) andTitle:@"" andFontNumber:14];
    
    [self.contentView addSubview:self.numberLabel];
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(5);
    }];
    
    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(30, 5, ScreenWidth - 50, 30) andTitle:@" 思想建设 (25分)" andFontNumber:13];
    
    self.titleLabel.numberOfLines = 0;
    
    [self.contentView addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(30);
        make.top.offset(5);
        make.width.mas_equalTo(ScreenWidth - 50);
    }];
    
    self.scoreTF = [[UITextField alloc] initWithFrame:CGRectMake(self.titleLabel.left, self.titleLabel.bottom + 5, 80, 30)];
    
    self.scoreTF.delegate = self;
    
    [self.contentView addSubview:self.scoreTF];
    
    [self.scoreTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.left.equalTo(_titleLabel.mas_left);
        make.bottom.offset(-5);
        make.width.mas_equalTo(80);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    self.scoreTF.borderStyle = UITextBorderStyleRoundedRect;
    
    UILabel *label = [UILabel customLablWithFrame:CGRectMake(self.scoreTF.right + 5, self.scoreTF.top, 30, 30) andTitle:@"分" andFontNumber:14];
    
    self.label = label;
    
    label.centerY = self.scoreTF.centerY;
    
    [self.contentView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.scoreTF.mas_centerY);
        make.left.equalTo(self.scoreTF.mas_right).offset(5);
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}
#warning 监听输入框
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    if ([textField.text integerValue] > self.choice.nScore) {
//        textField.text = [NSString stringWithFormat:@"%ld",(long)self.choice.nScore];
//    }else if([textField.text intValue] < 0) {
//        textField.text = @"0";
//    }else if([textField.text isEqualToString:@""] || textField.text == nil) {
//                textField.text = @"0";
//            }
////    if ([textField.text isEqualToString:@""] || textField.text == nil) {
////        textField.text = @"0";
////    }
//
//    self.ReviewedBlock(self.choice.nId,textField.text);
//}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//        if ([textField.text integerValue] > self.choice.nScore) {
//            textField.text = [NSString stringWithFormat:@"%ld",(long)self.choice.nScore];
//            
//            return NO;
//        }else if([textField.text intValue] <= 0) {
//            //textField.text = @"0";
//            return YES;
//        }
//        if ([textField.text isEqualToString:@""] || textField.text == nil) {
//            textField.text = @"0";
//            
//        }
//    
        //self.ReviewedBlock(self.choice.nId,string);
     self.ReviewedBlock(self.choice.nId,string);
    return YES;
}
//- (void)setOptionScore:(Optionscore *)optionScore {
//    if (_optionScore != optionScore) {
//        _optionScore = nil;
//        _optionScore = optionScore;
//        
//    }
//    if (optionScore == nil) {
//        
////        self.scoreTF.text = @"";
//        
//        self.scoreTF.enabled = YES;
//    }else {
//        
//        
//    }
//
//}

- (void)setOptionScore:(Optionscore *)optionScore andDic:(NSDictionary *)dic {
    
    self.optionScore = optionScore;
    
    if (optionScore != nil) {
        
       self.scoreTF.text = [NSString stringWithFormat:@"%ld",(long)optionScore.nScore];
       
        self.scoreTF.enabled = NO;
        
    }else if(optionScore == nil && dic[@(self.choice.nId)] != nil) {
#warning 服务器赋值乱行
       self.scoreTF.text = [NSString stringWithFormat:@"%@",dic[@(self.choice.nId)]];
//        self.scoreTF.text = [NSString stringWithFormat:@"%@",dic[@(self.choice.nScore)]];
//        self.scoreTF.text = [NSString stringWithFormat:@"%@",dic[@(self.reviewedModel.beGreadPerson[self.index].optionScore[self.index].nScore)]];
        self.scoreTF.enabled = YES;

    }
    else {
        
        self.scoreTF.text = @"";
        //self.scoreTF.text = nil;
        self.scoreTF.enabled = YES;
    }
    
}

- (void)setChoice:(ReviewedChoices *)choice {
    if (_choice != choice) {
        _choice = nil;
        _choice = choice;
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@ (%ld)",choice.vcOption,(long)choice.nScore];
        
//        self.titleLabel.frame = CGRectMake(30, 5, ScreenWidth - 50, [ReviewedCell CellheightWithStr:self.titleLabel.text] - 45);
//        self.titleLabel.textAlignment = NSTextAlignmentLeft;
//        [self.titleLabel sizeToFit];
        
//        self.scoreTF = [[UITextField alloc] initWithFrame:CGRectMake(self.titleLabel.left, self.titleLabel.bottom + 5, 80, 30)];
        
        
//        CGFloat height = [self stringHeightWithStr:self.titleLabel.text andsize:13 maxWidth:ScreenWidth - 50];
//        if (height > 30) {
//            self.titleLabel.height = height;
//            self.numberLabel.top = self.titleLabel.top;
//        self.scoreTF.top = self.titleLabel.bottom + 5;
//        self.label.centerY = self.scoreTF.centerY;
//
//        }

    }
}

- (void)setContent:(Content *)content {
    if (_content != content) {
        _content = nil;
        _content = content;
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@ (%ld分)",content.vcOption,(long)content.nOptionScore];
        [self.titleLabel sizeToFit];
        
        CGFloat height = [self stringHeightWithStr:self.titleLabel.text andsize:13 maxWidth:ScreenWidth - 50];
        if (height > 30) {
            self.titleLabel.height = height;
            self.numberLabel.top = self.titleLabel.top;
            self.scoreTF.top = self.titleLabel.bottom + 5;
            self.label.centerY = self.scoreTF.centerY;
        }
#warning 赋值
       

        self.scoreTF.text = [NSString stringWithFormat:@"%ld",(long)content.nScore];
        self.scoreTF.enabled = NO;
    }
}


- (float)stringHeightWithStr:(NSString *)str andsize:(CGFloat)fontSize maxWidth:(CGFloat)maxWidth {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:fontSize],NSFontAttributeName, nil];
    
    float height = [str boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    return ceilf(height);
}

+ (CGFloat)CellheightWithStr:(NSString *)content {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:14],NSFontAttributeName, nil];
    
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:14];
    
    label.text = content;
    label.width = ScreenWidth - 50;
    label.numberOfLines = 0;
    [label sizeToFit];
    
    float height = [content boundingRectWithSize:CGSizeMake(ScreenWidth - 50, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
//    if (height + 45 < 80) {
//        return 80;
//    }
    
    return 5 + label.height + 40;
}
@end
