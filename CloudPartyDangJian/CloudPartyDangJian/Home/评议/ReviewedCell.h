//
//  ReviewedCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReviewedListModel.h"
#import "MineModel.h"

@interface ReviewedCell : UITableViewCell
@property (nonatomic, strong) UITextField *scoreTF;
@property (nonatomic, strong) Optionscore *optionScore;
//@property (nonatomic, strong) ReviewedModel *reviewedModel;
@property (nonatomic, strong) ReviewedChoices *choice;
//传index值
@property (nonatomic, assign) NSInteger CellIndex;
@property (nonatomic, strong) UILabel *numberLabel;
//传数组
@property(nonatomic,strong)NSArray *optionScorearr;
//传赋值
@property (nonatomic, copy)NSString *scoreSTring;
@property (nonatomic, copy) void (^ReviewedBlock)(NSInteger nId, NSString *score);
@property(nonatomic,assign)NSIndexPath* indexpath;
@property (nonatomic, strong) ReviewedModel *CellreviewedModel;


/**
 *  我的评议模块 model
 */
@property (nonatomic, strong) Content *content;

- (void)setOptionScore:(Optionscore *)optionScore andDic:(NSDictionary *)dic;

+ (CGFloat)CellheightWithStr:(NSString *)content;

@end
