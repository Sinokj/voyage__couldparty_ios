//
//  HomeController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "HomeController.h"
#import <Masonry.h>
#import "HomeHead.h"
#import "HomeModel.h"
#import "HomeCell.h"
#import "ViwepagerModel.h"
#import "CollectViewModel.h"
#import "WebViewController.h"
#import "ChooseDangZuController.h"//选择党委

#import <SDCycleScrollView.h>
#import "ListController.h"//新闻列表
#import "ViwepagerModel.h"
#import "LoginController.h"
#import "ExamListController.h"//在线考试
//投票
#import "VoteListController.h"
//评议
#import "ReviewedListController.h"


#import "PartyArticleListController.h"//三会一课

#import "SuggestsController.h"
#import "SuggestController.h"//意见征集

#import "PayMoneyViewController.h"
#import "DemocracySuggestViewController.h"
#import "QuestionsSurveyViewController.h"
#import "ExaminationOnlineViewController.h"
#import "JPUSHService.h"
#import "PublicityViewController.h"
#import "LLSearchViewController.h"
#import "AppDelegate.h"
#import "BulletinController.h"
@interface HomeController ()<SDCycleScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

//@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, strong) ViwepagerModel *viweoagerM;
@property (nonatomic, strong) CollectViewModel *collectModel;
@property (nonatomic, strong) NSMutableArray *picArr;
@property (nonatomic, strong) NSMutableArray *cycleTitleArr;
@property (nonatomic, strong) UICollectionView *collectV;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout2;
@property (nonatomic, strong) NSMutableArray *sourceArr;
//@property (nonatomic, strong) HomeModel *homeModel;
@property (nonatomic, strong) NSMutableArray *titleArr;
@property (nonatomic, assign) BOOL isShow;
@property (nonatomic, strong) UIImageView *barImageView;

@property (nonatomic, strong) NSMutableArray *lunboDataArray;
@property (nonatomic, strong) NSMutableArray *collectDataArray;
@property (nonatomic, assign) NSInteger itemType;//设置3x或4x
@property (nonatomic, strong) NSString *partyIDStr;

@property (nonatomic, strong) NSMutableArray *articles;


@end

@implementation HomeController
- (instancetype)init{
    if (self = [super init]) {
        //监听登录状态
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"isLoginUpdateData" object:nil];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"isLoginUpdateData" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.collectV];
    
    [self.collectV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.right.left.equalTo(self.view);
        make.bottom.offset(0);

    }];
    
    [self setupLeftItem];

    [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:10 ToView:self.view userInteractionEnabled:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    self.isShow = YES;
    
    [self requestData];
}

//设置导航栏标题
- (void)setSelfViewTitleWith:(NSString *)titleStr {
    
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.text = titleStr;
    titleLab.textColor = white;
    titleLab.font = FontSystem(MYDIMESCALEW(16));
    [titleLab sizeToFit];
    
    self.navigationItem.titleView = titleLab;
}

- (void)setupLeftItem {

    @weakify(self);
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"navi_left2" block:^(id sender) {
        
        
        [weak_self chooseDangZuZhi];
    }];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"home_search2" block:^(id sender) {
        [weak_self pushToSearchVC];
    }];
    
}

- (void)pushToSearchVC {
    
    LLSearchViewController *vc = [[LLSearchViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:true];
    
}

- (void)chooseDangZuZhi {
    ChooseDangZuController *CDZC = [[ChooseDangZuController alloc] init];
    
    __weak typeof(self) weakSelf = self;
    CDZC.PartyBlock = ^(id model) {
        weakSelf.itemType = [[model valueForKey:@"nModuleType"] integerValue];
        weakSelf.partyIDStr = [model valueForKey:@"nId"];
        [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:10 ToView:self.view userInteractionEnabled:YES];
        [weakSelf updateGroupInfo];
    };
    
    [self.navigationController pushViewController:CDZC animated:YES];
    
}

- (void)updateInfoAfterLoginIn:(NSString *)partyIDStr {
    self.partyIDStr = partyIDStr;
    //    [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:10 ToView:self.view userInteractionEnabled:YES];
    //    [self updateGroupInfo];
    
}
- (void)updateGroupInfo {
    
    [STUtils setObject:self.partyIDStr forKey:KDangJianType];
    
    [self refreshTheData];
    
    TabbarController *tabBarVc = [(AppDelegate *)[UIApplication sharedApplication].delegate tabBarVc];
    UINavigationController *nav = (UINavigationController *)(tabBarVc.viewControllers[1]);
    
    BulletinController *bvc = nav.viewControllers[0];
    // 刷新新闻的数据
    [bvc reloadData];
}

//[MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:10 ToView:self.view userInteractionEnabled:YES];
- (void)requestData {
    
    NSString *url;
    self.partyIDStr = [STUtils objectForKey:@"partyID"];
    
    if (self.partyIDStr == nil) {
        url = [NSString stringWithFormat:@"%@?nCommitteeId=%@", HomeURL, @2];// 默认是2
    }
    else {
        url = [NSString stringWithFormat:@"%@?nCommitteeId=%@", HomeURL, self.partyIDStr];
    }
    
    [DataRequest PostWithURL:url parameter:nil andImage:nil :^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
        [self.picArr removeAllObjects];
        [self.cycleTitleArr removeAllObjects];
        [self.titleArr removeAllObjects];
        [self.collectDataArray removeAllObjects];
        [self.articles removeAllObjects];
        
        NSDictionary *baseDic = responseObject[@"objects"];
        //滚动图
        NSArray *lunbotuArr = baseDic[@"articleList"];
        if (lunbotuArr.count > 0) {
            [self.articles addObjectsFromArray:[ArticleListModel mj_objectArrayWithKeyValuesArray:lunbotuArr]];
        }
        
        for (NSDictionary *dic in lunbotuArr) {
            NSString *imgURL = dic[@"vcPath"];
            [self.picArr addObject:imgURL];
            NSString *lunboTitle = dic[@"vcTitle"];
            [self.cycleTitleArr addObject:lunboTitle];
            
            self.viweoagerM = [ViwepagerModel mj_objectWithKeyValues:@{@"objects":lunbotuArr}];
            //                [self.collectV reloadData];
        }
        
        //collectView
        NSArray *collectArr = baseDic[@"moduleList"];
        
        [self.titleArr removeAllObjects];
        
        for (NSDictionary *dic in collectArr) {
            [self.titleArr addObject:dic[@"vcModule"]];
            //                [self.collectDataArray addObject:[CollectViewModel createModelWithDict:dic]];
            self.collectModel = [CollectViewModel mj_objectWithKeyValues:@{@"objectX":collectArr}];
            
        }
        //            [self.collectV reloadData];
        
        //获取3x或4x
        NSDictionary *dic = baseDic[@"committee"];
        self.itemType = [dic[@"nModuleType"] integerValue];
        //设置标题
        [self setSelfViewTitleWith:[NSString stringWithFormat:@"  %@ ", dic[@"vcPartyAppName"]]];
        
        //获取3x或4x之后进行页面布局
        [self prepareLayout];
        
        [MBProgressHUD hideHUD];
    } failure:^(NSError *error) {
        
    }];
//
//    [DataRequest PostWithURL:url parameter:nil :^(id reponserObject) {
//
//
//    }];
    
    
    //        [self changgeVersions];
    
}




- (void)prepareLayout {
    
    //导航条背景透明
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"home_search2"] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] style:(UIBarButtonItemStylePlain) target:self action:@selector(pushToSearchVC)];
    
    
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
    if (self.itemType == 3) {
         [self.collectV reloadData];
        self.collectV.collectionViewLayout = self.flowLayout;
    }
    else if (self.itemType == 4) {
        [self.collectV reloadData];
        self.collectV.collectionViewLayout = self.flowLayout2;
    }
    
    //    });
    
}
- (void)refreshTheData {
    
    //    [self prepareLayout];
    [self requestData];
    [self.collectV.mj_header endRefreshing];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //    self.navigationController.navigationBar.translucent = NO;
    //    self.navigationController.navigationBar.barTintColor = AppTintColor;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //    return self.collectModel.objectX.count;
    return self.titleArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCell" forIndexPath:indexPath];
    //设置数据
    ObjectsX *dic = self.collectModel.objectX[indexPath.row];
    NSString *iconURL = [dic valueForKey:@"vcIconUrl"];
    
    [cell.img sd_setImageWithURL:[NSURL URLWithString:iconURL]];
    
    if (self.isShow) {
        if (self.titleArr.count > 0) {
            [cell setModel:self.collectModel andTitle:self.titleArr[indexPath.item]];
        }
        
    }else {
        
        [cell setModel:nil andTitle:self.titleArr[indexPath.item]];
    }
    
    if (self.itemType == 3) {
        @weakify(cell);
        cell.titleLab.font = FontSystem(MYDIMESCALEW(14));
        [cell.img mas_updateConstraints:^(MASConstraintMaker *make) {
            //            make.top.offset(MYDIMESCALEH(22));
            make.width.height.mas_equalTo(MYDIMESCALEW(60));
            make.centerX.equalTo(weak_cell.contentView.mas_centerX);
        }];
    }else {
        @weakify(cell);
        cell.titleLab.font = FontSystem(MYDIMESCALEW(12));
        [cell.img mas_updateConstraints:^(MASConstraintMaker *make) {
            //            make.top.offset(MYDIMESCALEH(22));
            make.width.height.mas_equalTo(MYDIMESCALEW(40));
            make.centerX.equalTo(weak_cell.contentView.mas_centerX);
        }];
    }
    
    
    return cell;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    HomeHead *headReusableView;
    if (kind == UICollectionElementKindSectionHeader) {
        headReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"head" forIndexPath:indexPath];
        
        headReusableView.cycleScrollView.delegate = self;
        headReusableView.cycleScrollView.autoScrollTimeInterval = 3;
        headReusableView.cycleScrollView.titlesGroup = self.cycleTitleArr;
        headReusableView.cycleScrollView.titleLabelBackgroundColor = [UIColor colorWithWhite:0 alpha:0.45];
        headReusableView.cycleScrollView.imageURLStringsGroup = self.picArr;
    }
    return headReusableView;
}

//点击cell实现跳转
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title = self.titleArr[indexPath.item];
    
    ObjectsX *objectX = self.collectModel.objectX[indexPath.row];;
    NSInteger nTemplate = [[objectX valueForKey:@"nTemplate"] integerValue];
    DLog(@"nTemplate:%ld", nTemplate);//模板类型字段
    
    NSInteger nModuleId = [[objectX valueForKey:@"nId"] integerValue];//模块Id
    
    if ([objectX.vcModule isEqualToString:@"公示公告"]) {
        if (![STUtils isLogin]) {
            [MBProgressHUD showMessage:@"请登录" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
            return;
        }
    }
    
    switch (nTemplate) {
        case 1: {  //新闻
            
            [self pushWithType:self.titleArr[indexPath.item] withNModuleId:nModuleId];
        }
            break;
        case 2: { //答题
            
            ExamListController *examList = [ExamListController new];
            examList.hidesBottomBarWhenPushed = YES;
            examList.title = objectX.vcModule;
            examList.type = objectX.vcModule;
            examList.vcModule = objectX.vcModule;
            examList.isFirst = YES;
            //                examList.reponsObjects = reponserObject;
            [self.navigationController pushViewController:examList animated:YES];
        }
            break;
        case 3: {  //H5
            
            WebViewController *w = [WebViewController new];
            w.type = @"1";
            w.tit = objectX.vcModule;
            w.str = objectX.vcJumpLink;
            //            if (objectX.nId == 63) {
            //                w.nID = objectX.nId;
            //            }
            [self.navigationController pushViewController:w animated:YES];
            break;
            
        }
            break;
        case 4: {  //意见征集
            if (![STUtils isLogin]) {
                [MBProgressHUD showMessage:@"请登录" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
                return;
            }
            
            SuggestController *SC = [[SuggestController alloc] init];
            [self.navigationController pushViewController:SC animated:YES];
            
        }
            break;
        case 5: {
            if ([title isEqualToString:@"三会一课"]) {
                [DataRequest GetWithURL:[NSString stringWithFormat:@"%@", SanHuiYiKeMark] :^(id reponserObject) {
                    
                    PartyArticleListController *PALC = [[PartyArticleListController alloc] init];
                    PALC.responsObject = reponserObject;
                    PALC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:PALC animated:YES];
                    
                }];
                
                return;
            }
            
            
        }
            break;
            
        default:
            break;
    }
    
    
    
    
    //democryacySuggest，在base里拼接
    if ([title isEqualToString:@"民主评议"]) {
        [DataRequest GetWithURL:democryacySuggest :^(id reponserObject) {
            //            ReviewedListController *reviewvc = [[ReviewedListController alloc] init];
            //            reviewvc.hidesBottomBarWhenPushed = YES;
            //            [self.navigationController pushViewController:reviewvc animated:YES];
            //    DemocracySuggestViewController *demory = [[DemocracySuggestViewController alloc] init];
            //            demory.hidesBottomBarWhenPushed = YES;
            //            [self.navigationController pushViewController:demory animated:YES];
            //
            UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            [item setTintColor:[UIColor whiteColor]];
            self.navigationItem.backBarButtonItem = item;
            
        }];
        return;
    }
    if ([title isEqualToString:@"问卷调查"]) {
        [DataRequest GetWithURL:questionSurvey :^(id reponserObject) {
            //            QuestionsSurveyViewController *questionVC = [[QuestionsSurveyViewController alloc] init];
            //            questionVC.hidesBottomBarWhenPushed = YES;
            //            [self.navigationController pushViewController:questionVC animated:YES];
        }];
        return;
    }
}


- (UILabel *)customLablWithFrame:(CGRect)frame andTitle:(NSString *)text andFontNumber:(NSInteger)fontNumber {
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = LabelBgColor;
    label.text = text;
    label.font = FontSystem(fontNumber);
    
    return label;
}

- (void)customImageVWithView:(UIView *)superView andBottom:(CGFloat)bottom andArray:(NSArray *)picArr andBaseTag:(NSInteger)baseTag {
    for (int i = 0; i < picArr.count; i++) {
        
        UIImageView *imageV = [[UIImageView alloc] init];
        CGFloat top = i/4 >= 1 ? (ScreenWidth/4 + 10) * (i/4) + bottom + 1 : (ScreenWidth/4) * (i/4) + bottom;
        
        imageV.frame = CGRectMake(ScreenWidth/4 * (i%4), top, ScreenWidth/4 - 1, ScreenWidth/4 + 10);
        
        if (![picArr[i] isEqualToString:@""]) {
            imageV.image = [UIImage imageNamed:picArr[i]];
        }
        imageV.tag = baseTag + i;
        imageV.backgroundColor = [UIColor whiteColor];
        imageV.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        
        [imageV addGestureRecognizer:tap];
        [superView addSubview:imageV];
    }
}


/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    ArticleListModel *listModel = self.articles[index];
    if (listModel) {
        
        WebViewController *web = [WebViewController new];
        web.type = @"1";
        //    DLog(@"%@ -------- %d ----- %d",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject],self.viweoagerM.objects[index].vcUrl == nil , [self.viweoagerM.objects[index].vcUrl isEqualToString:@""]);
        
        
        web.tit =  @"要闻";// self.viweoagerM.objects[index].vcTitle;
        //    web.shareImage = self.viweoagerM.objects[index].vcPath;
        web.shareTitle = listModel.vcTitle;
        web.str = listModel.url;
        web.shareDescribe = listModel.vcDescribe;
        
        web.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:web animated:YES];
    }
    
}

- (void)pushWithType:(NSString *)type withNModuleId:(NSInteger)nModuleId {
    
    NSUserDefaults *userDe = [NSUserDefaults standardUserDefaults];
    NSString *nCommitteeId = [userDe objectForKey:@"partyID"];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [DataRequest PostWithURL:[NSString stringWithFormat:@"%@?vcType=%@&nPageNo=1&nPageSize=10&nCommitteeId=%@&nTagId=0", ArticleURL, type, nCommitteeId] parameter:nil :^(id reponserObject) {
            
            ListController *list = [ListController new];
            list.nModuleId = nModuleId;
            list.type = type;
            list.listM = [ListModel mj_objectWithKeyValues:reponserObject];
            
            list.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:list animated:YES];
            
        }];
        
    });
    
}

//获取版本号
-(void)changgeVersions{
    [DataRequest GetAppTest:^(id responseObject) {
        NSArray* tempArr = responseObject[@"results"];
        if (tempArr.count>0) {
            NSDictionary* infoDic = [[NSBundle mainBundle] infoDictionary];
            NSString* currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];//当前版本号
            NSString* localVersion = responseObject[@"results"][0][@"version"];
            if ([localVersion compare:currentVersion] == NSOrderedDescending) {
                
                UIAlertController* alertControl = [UIAlertController alertControllerWithTitle:@"提示" message:@"您有新版本,请前往 AppStore 更新" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    //DLog(@"稍后再说");
                }];
                
                [alertControl addAction:cancel];
                [self presentViewController:alertControl animated:YES completion:nil];
            }
        }
    }];
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _flowLayout.minimumInteritemSpacing = 1;
        _flowLayout.itemSize = CGSizeMake((ScreenWidth - 2)/3, (ScreenWidth - 2)/3 - 5);
        _flowLayout.minimumLineSpacing = 1;
        _flowLayout.headerReferenceSize = CGSizeMake(ScreenWidth, 200);
    }
    return _flowLayout;
}
- (UICollectionViewFlowLayout *)flowLayout2 {
    if (!_flowLayout2) {
        _flowLayout2 = [[UICollectionViewFlowLayout alloc] init];
        _flowLayout2.minimumInteritemSpacing = 1;
        _flowLayout2.itemSize = CGSizeMake((ScreenWidth - 3)/4, (ScreenWidth - 3)/4);
        _flowLayout2.minimumLineSpacing = 1;
        _flowLayout2.headerReferenceSize = CGSizeMake(ScreenWidth, 210);
        
    }
    return _flowLayout2;
}

- (UICollectionView *)collectV {
    if (!_collectV) {
        _collectV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
        
        
        _collectV.backgroundColor = [UIColor whiteColor];
        _collectV.delegate = self;
        _collectV.dataSource = self;
        
        [_collectV registerClass:[HomeHead class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectV registerClass:[HomeCell class] forCellWithReuseIdentifier:@"HomeCell"];
        
        if (@available(iOS 11.0, *)) {
            
        } else {
            // Fallback on earlier versions
        }
        
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshTheData)];
        [header setTitle:@"松手即可刷新" forState:MJRefreshStatePulling];
        _collectV.mj_header = header;
    }
    return _collectV;
}
- (NSMutableArray *)articles {
    if (!_articles) {
        _articles = [NSMutableArray array];
    }
    return _articles;
}

- (NSMutableArray *)picArr {
    if (_picArr == nil) {
        _picArr = [NSMutableArray array];
    }
    return _picArr;
}
- (NSMutableArray *)lunboDataArray {
    if (_lunboDataArray == nil) {
        _lunboDataArray = [NSMutableArray array];
    }
    return _lunboDataArray;
}
- (NSMutableArray *)collectDataArray {
    if (_collectDataArray == nil) {
        _collectDataArray = [NSMutableArray array];
    }
    return _collectDataArray;
}
- (NSMutableArray *)titleArr {
    if (_titleArr == nil) {
        _titleArr = [NSMutableArray array];
    }
    return _titleArr;
}
- (NSMutableArray *)cycleTitleArr {
    if (!_cycleTitleArr) {
        _cycleTitleArr = [NSMutableArray array];
    }
    return _cycleTitleArr;
}

@end

