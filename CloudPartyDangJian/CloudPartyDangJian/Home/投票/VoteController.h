//
//  VoteController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "VoteModels.h"

@interface VoteController : ViewController

@property (nonatomic, strong) VoteList *voteModel;

@end
