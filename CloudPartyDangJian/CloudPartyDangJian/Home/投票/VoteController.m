//
//  VoteController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "VoteController.h"
#import "VoteControllerCell.h"

@interface VoteController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *voteTableV;

@end

@implementation VoteController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"投票";
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
    
}

- (void)prepareLayout {
    
    UILabel *titleL = [UILabel customLablWithFrame:CGRectMake(10, 20, ScreenWidth - 20, 40) andTitle:self.voteModel.vcTopic andFontNumber:14];
    
    [self.view addSubview:titleL];
    
    self.voteTableV = [[UITableView alloc] initWithFrame:CGRectMake(0, titleL.bottom, ScreenWidth, 260 * ScreenWidth/320) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.voteTableV];
    
    self.voteTableV.delegate = self;
    
    self.voteTableV.dataSource = self;
    
//    self.voteTableV.scrollEnabled = NO;
    
    self.voteTableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.voteTableV registerClass:[VoteControllerCell class] forCellReuseIdentifier:@"VoteControllerCell"];
    
    UIButton *sendBtn = [UIButton buttonWithType:(UIButtonTypeSystem)];
    
    [self.view addSubview:sendBtn];
    
    sendBtn.frame = CGRectMake(30, self.voteTableV.bottom + 30, ScreenWidth - 60, 40);
    
    [sendBtn setTitle:@"提  交" forState:(UIControlStateNormal)];
    
    [sendBtn setTintColor:[UIColor whiteColor]];
    
    [sendBtn setBackgroundColor: [UIColor colorWithRed:0.9463 green:0.1012 blue:0.1432 alpha:1.0]];
    
    sendBtn.layer.cornerRadius = 3;
    
    [sendBtn addTarget:self action:@selector(sendBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    if (self.voteModel.isVoted.isVoted != 1) {
        
        sendBtn.hidden = NO;
        
    }else {
        
        sendBtn.hidden = YES;
    }

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.voteModel.choices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VoteControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VoteControllerCell" forIndexPath:indexPath];
    
    if (self.voteModel.isVoted.isVoted == 1) {
        NSArray *arr =[self.voteModel.isVoted.voteOptions componentsSeparatedByString:@","];

        if ([arr containsObject:[NSString stringWithFormat:@"%ld",self.voteModel.choices[indexPath.row].nOptionId]]) {
            self.voteModel.choices[indexPath.row].isSelect = YES;
        }

    }

    
    
    cell.voteChoices = self.voteModel.choices[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.voteModel.isVoted.isVoted == 1) {
        
    }else {
        VoteControllerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell selectBtnClickWithRow:indexPath.row andModel:self.voteModel];
    }
    [self.voteTableV reloadData];
    
}

- (void)sendBtnClick {
    
    NSMutableString *selec = [NSMutableString string];
    
    for (VoteChoices *choice in self.voteModel.choices) {
        
        if (choice.isSelect) {
            
            [selec appendFormat:@"%ld,",choice.nOptionId];
            
        }
    }
    
    if (selec == nil && [selec isEqualToString:@""]) {
        
        [MBProgressHUD showMessage:@"请先选择再提交" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
    }
    
    [DataRequest PostWithURL:SendVote parameter:@{@"nTopicsId":@(self.voteModel.nId) ,@"vcVoteOptions":selec } :^(id reponserObject) {
        
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            [MBProgressHUD showMessage:@"提交成功" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else {
            
            [MBProgressHUD showMessage:@"提交失败" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
            
        }

    }];
}


@end
