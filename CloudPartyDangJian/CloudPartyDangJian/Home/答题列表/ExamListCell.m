//
//  ExamListCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ExamListCell.h"
#import "DyfTool.h"

@interface ExamListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/**
 *  状态
 */
@property (weak, nonatomic) IBOutlet UIImageView *stateLabel;

/**
 *  已答
 */
@property (weak, nonatomic) IBOutlet UILabel *haveDoneL;

/**
 *  几人作答
 */
@property (weak, nonatomic) IBOutlet UILabel *numberL;

/**
 状态标志
 */
@property (weak, nonatomic) IBOutlet UIImageView *stateImageV;

/**
 答题数量标志
 */
@property (weak, nonatomic) IBOutlet UIImageView *markImageView;

@end
@implementation ExamListCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.contentView.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, ScreenWidth - 6, 80)];
    
    [self.contentView addSubview:bgView];
    
    bgView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
    
    imageV.image = [UIImage imageNamed:@"ic_doc"];
    
    [bgView addSubview:imageV];
    
    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(imageV.right + 10, 8, ScreenWidth - 120, 20) andTitle:@"测试" andFontNumber:15];
    
    [bgView addSubview:self.titleLabel];
    
    
//    self.stateLabel = [UILabel customLablWithFrame:CGRectMake(self.titleLabel.right, 8, 50, 20) andTitle:@"未完成" andFontNumber:15];
//    
//    self.stateLabel.textAlignment = NSTextAlignmentRight;
//    
    [bgView addSubview:self.stateLabel];
    
    self.haveDoneL = [UILabel customLablWithFrame:CGRectMake(10, imageV.bottom + 10, ScreenWidth / 2 - 20, 20) andTitle:@"已答" andFontNumber:13];
    
    [bgView addSubview:self.haveDoneL];
    
    
    self.numberL = [UILabel customLablWithFrame:CGRectMake(self.haveDoneL.right, self.haveDoneL.top, 80, 20) andTitle:@"未完成" andFontNumber:13];
    
    self.numberL.textAlignment = NSTextAlignmentRight;
    
    [bgView addSubview:self.numberL];
}

- (void)setExamLists:(ExamLists *)examLists {
    if (_examLists != examLists) {
        _examLists = nil;
        _examLists = examLists;
        
        self.titleLabel.text = examLists.vcTitle;
        
        [self setAttributedStr:[NSString stringWithFormat:@"%ld/%ld",(long)examLists.answerdNumber,(long)examLists.nTopicsTotal] andFrome:0 andLength:[[NSString stringWithFormat:@"%ld",(long)examLists.answerdNumber] length] withLabel:self.haveDoneL];
        
         [self setAttributedStr:[NSString stringWithFormat:@"%ld 人作答",(long)examLists.examedNumber] andFrome:0 andLength:[[NSString stringWithFormat:@"%ld",(long)examLists.examedNumber] length] withLabel:self.numberL];
        
        [self.contentView bringSubviewToFront:self.stateLabel];
        if (examLists.isBegin == 2) {
            self.stateLabel.image = [UIImage imageNamed:@"label_complete"];
        }else if(examLists.isBegin == 3){
            self.stateLabel.image = [UIImage imageNamed:@"party_end"];
        }else{
            self.stateLabel.image = [UIImage imageNamed:@"label_onway"];
        }
//        switch (examLists.isBegin) {
//            case 0:
//                self.stateLabel.image = [UIImage imageNamed:@"label_onway"];
//                self.stateImageV.image = [UIImage imageNamed:@"icon_being"];
//                break;
//            case 1:
//
//                self.stateLabel.image = [UIImage imageNamed:@"label_onway"];
//                self.stateImageV.image = [UIImage imageNamed:@"icon_being"];
//                break;
//            case 2:
//
//                self.stateLabel.image = [UIImage imageNamed:@"label_complete"];
//                self.stateImageV.image = [UIImage imageNamed:@"icon_complete"];
//                break;
//            case 3:
//
//                self.stateLabel.image = [UIImage imageNamed:@"label_complete"];
//                self.stateImageV.image = [UIImage imageNamed:@"party_end"];
//                break;
//            default:
//                break;
//        }
    }
}


- (void)setAttributedStr:(NSString *)str andFrome:(NSInteger)start andLength:(NSInteger)length withLabel:(UILabel *)label {
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSDictionary *dic = @{ NSForegroundColorAttributeName:COLOR_RGB(0xffd700)};
    
    [attributedString addAttributes:dic range:NSMakeRange(start, length)];
    
    label.attributedText = attributedString;
    
}



@end
