//
//  ExamListCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExamListModel.h"

@interface ExamListCell : UITableViewCell

@property (nonatomic, strong) ExamLists *examLists;
@end
