//
//  ExamListController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ExamListController.h"
#import "ExamListCell.h"
#import "ExamListModel.h"
#import "PrepareExamController.h"

@interface ExamListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) ExamListModel *examListM;


@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *modelArray;
@end

@implementation ExamListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareLayout];
    
//    [self.tableV.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //默默刷新页面
    [self.tableV.mj_header beginRefreshing];
}

- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kTopHeight - kBottomSafeAreaHeight) style:(UITableViewStylePlain)];
    [self.view addSubview:self.tableV];
    
    self.tableV.backgroundColor = UIColorFromHex(0xE3E3E3);
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableV registerNib:[UINib nibWithNibName:@"ExamListCell" bundle:nil] forCellReuseIdentifier:@"ExamListCell"];
    
    MJWeakSelf;
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [weakSelf requestForAnswersList];
    }];
    
    self.tableV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [weakSelf requestForAnswersList];
    }];
}

#pragma mark - net
- (void)requestForAnswersList{
    NSString *partyID = [STUtils objectForKey:KDangJianType];
    [DataRequest CatlikeGetWithURL:[NSString stringWithFormat:@"%@?vcClassify=%@&nCommitteeId=%@&pageNum=%ld",ExamList,self.vcModule, partyID,self.page] :^(id reponserObject) {
        [self.tableV.mj_header endRefreshing];
        [self.tableV.mj_footer endRefreshing];
        
        NSArray *temp = [ExamLists mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]];
        if(temp.count == 0 && self.page > 1) {
            return ;
        }
        if (self.page == 1) {
            [self.modelArray removeAllObjects];
        }
        [self.modelArray addObjectsFromArray:temp];
        [self.tableV reloadData];
        
     
    }];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ExamListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExamListCell" forIndexPath:indexPath];
    
    cell.examLists = self.modelArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    PrepareExamController *prepareExam = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PrepareExamController"];
    
    prepareExam.examList = self.modelArray[indexPath.row];

    [self.navigationController pushViewController:prepareExam animated:YES];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return MYDIMESCALEH(100);
}


#pragma mark - lazy
- (NSMutableArray *)modelArray{
    if (_modelArray == nil) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

@end
