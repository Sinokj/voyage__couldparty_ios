//
//  ExamListController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"

@interface ExamListController : ViewController

@property (nonatomic, strong) id reponsObjects;

@property (nonatomic, strong) NSString *type;

@property (nonatomic, assign) BOOL isFirst;

@property (nonatomic, copy) NSString *vcModule;

@end
