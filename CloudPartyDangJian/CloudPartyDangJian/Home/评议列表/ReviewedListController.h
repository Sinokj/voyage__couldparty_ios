//
//  ReviewedListController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "ReviewedListModel.h"

@interface ReviewedListController : ViewController

@property (nonatomic, strong) ReviewedListModel *reviewedListModel;

@property (nonatomic, assign) BOOL isFirst;

@end
