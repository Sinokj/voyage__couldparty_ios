//
//  HomeHead.m
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/9/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "HomeHead.h"
#import "DyfTool.h"

@implementation HomeHead

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, ScreenWidth, 0)  delegate:nil placeholderImage:nil];
    
//    _cycleScrollView.delegate = self;
    
    [self addSubview:_cycleScrollView];
    
    _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    
    _cycleScrollView.titleLabelBackgroundColor = [UIColor clearColor];
    
    _cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    
    NSArray *arr = [NSArray arrayWithObjects:[UIImage imageNamed:@"Banner_1.png"],[UIImage imageNamed:@"Banner_2.png"],[UIImage imageNamed:@"Banner_3.png"], nil];
    
    _cycleScrollView.localizationImageNamesGroup = arr;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _cycleScrollView.height = self.height;
}

@end
