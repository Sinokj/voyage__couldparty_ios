//
//  appraiseModel.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/12.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface appraiseModel : NSObject
@property (nonatomic, copy) NSString *writeText;
- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)momentWithDict:(NSDictionary *)dict;
@end
