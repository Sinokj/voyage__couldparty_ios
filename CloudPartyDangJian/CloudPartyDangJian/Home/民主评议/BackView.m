//
//  BackView.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/7.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "BackView.h"
#import "DataRequest.h"
#import "HeaderUrl.h"
#import <Masonry.h>
#import <MJExtension.h>

@interface BackView()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *menutabView;
//第一层数组
@property(nonatomic,strong)NSMutableArray *objectM;
//第二层数组
@property(nonatomic,strong)NSMutableArray *greadpersonM;
//最终数组
@property(nonatomic,strong)NSMutableArray *endArr;
@end
@implementation BackView
-(NSMutableArray *)objectM{
    if (_objectM == nil) {
        _objectM = [NSMutableArray array];
    }
    return _objectM;
}
-(NSMutableArray *)greadpersonM{
    if (_greadpersonM == nil) {
        _greadpersonM = [NSMutableArray array];
    }
    return _greadpersonM;
}

-(NSMutableArray *)endArr{
    if (_endArr == nil) {
        _endArr = [NSMutableArray array];
    }
    return _endArr;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
                UITableView *menutab = [[UITableView alloc] initWithFrame:CGRectMake(10, 10, 350, 600) style:UITableViewStylePlain];
        self.menutabView = menutab;
        menutab.separatorInset = UIEdgeInsetsZero;
        UIView *headerView = [[UIView alloc] init];
        //headerView.backgroundColor = [UIColor purpleColor];
        headerView.frame = CGRectMake(100, 200, 0, 50);
        UILabel *menuLabel = [[UILabel alloc] init];
        menuLabel.text = @"选择评议人员";
        [menuLabel setTextColor:[UIColor blueColor]];
        [headerView addSubview:menuLabel];
        [menuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.offset(10);
        }];
        UIView *blueView = [[UIView alloc] init];
        blueView.backgroundColor = [UIColor blueColor];
        [headerView addSubview:blueView];
        [blueView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
            make.left.right.offset(0);
            make.height.offset(1);
        }];
        menutab.tableHeaderView = headerView;
        menutab.delegate = self;
        menutab.dataSource = self;
        [menutab registerClass:[UITableViewCell class] forCellReuseIdentifier:@"menuCellId"];
        
                //menutab.allowsSelectionDuringEditing = YES;
                [self addSubview:menutab];
    
        [self reloadData];
    }
    return self;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _endArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCellId" forIndexPath:indexPath];
    self.greadpersonModel = _endArr[indexPath.row];
 
    cell.textLabel.text = self.greadpersonModel.BEGreadPerson
    ;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.callBackBlock !=nil) {
       self.greadpersonModel = _endArr[indexPath.row]; self.callBackBlock(self.greadpersonModel.BEGreadPerson);
    }
    if (self.reloaddataBlock != nil) {
                self.reloaddataBlock();
    }
    [self removeFromSuperview];
}
-(void)returnText:(CallBackBlcok)block{
    self.callBackBlock = block;
}
-(void)returnScore:(reloadDataBlock)block{
    self.reloaddataBlock = block;
}
-(void)reloadData{
    [DataRequest GetWithURL:democryacySuggest :^(id reponserObject) {
        _objectM = [[NSMutableArray alloc] init];
        [_objectM addObjectsFromArray:reponserObject[@"objects"]];
        for (int i = 0; i < self.objectM.count; i++) {
            NSDictionary *dict = self.objectM[i];
            self.thoughtM = [thought mj_objectWithKeyValues:dict];
            DLog(@"%@",self.thoughtM);
            _greadpersonM= [[NSMutableArray alloc] init];
            [_greadpersonM addObjectsFromArray:self.thoughtM.beGreadPerson];
                    }
        DLog(@"%@",_greadpersonM);
        _endArr = [[NSMutableArray alloc] init];
        NSArray *arr= [GreadPerson mj_objectArrayWithKeyValuesArray:_greadpersonM];
        for (NSMutableArray *endArr in arr) {
            [_endArr addObject:endArr];
        }
        DLog(@"哈哈11%@",_endArr);
        [self.menutabView reloadData];
    }];
}

@end
