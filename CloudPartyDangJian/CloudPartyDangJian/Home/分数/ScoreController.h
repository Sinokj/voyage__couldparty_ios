//
//  ScoreController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"

@interface ScoreController : ViewController

@property (nonatomic, assign) NSInteger nScore;

@property (nonatomic, strong) NSString *isPass;

@end
