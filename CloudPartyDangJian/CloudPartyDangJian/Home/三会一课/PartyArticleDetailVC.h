//
//  PartyArticleDetailVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "ViewController.h"
#import "PartyArticleListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PartyArticleDetailVC : ViewController
@property (nonatomic, assign)NSInteger nId;
@property(nonatomic, copy)NSString *type;
@property (nonatomic, assign)BOOL isDisPlayBtm;
@end

NS_ASSUME_NONNULL_END
