//
//  PartySubVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "PartySubVC.h"
#import "PartyArticleListModel.h"
#import "SanHuiYiKeListCell.h"
#import "PartyArticleDetailVC.h"

@interface PartySubVC ()

@end

@implementation PartySubVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //绘制页面
    [self drawSubView];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)drawSubView{
    [self.view addSubview:self.tableView];
}

#pragma mark - net
- (void)requestForSaleGongdanList{
    NSUserDefaults *userDe = [NSUserDefaults standardUserDefaults];
    NSString *partyID = [userDe objectForKey:@"partyID"];
    
    NSString *url = [NSString stringWithFormat:@"%@nCommitteeId=%@&vcType=%@&page=%ld&rows=8",SanHuiYiKeList, partyID?partyID:@2, self.yp_tabItemTitle, self.page];
    
    [DataRequest GetWithURL:url :^(id reponserObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        NSArray *models = [PartyArticle mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]];
        if(models.count == 0 && self.page > 1) {
            return ;
        }
        
        if (self.page == 1) {
            [self.modelArray removeAllObjects];
        }
        
        [self.modelArray addObjectsFromArray:models];
        [self.tableView reloadData];
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SanHuiYiKeListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SanHuiYiKeListCell" owner:nil options:nil][0];
    }
    cell.model = self.modelArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return MYDIMESCALEH(100);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SanHuiYiKeListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            cell.stateBtn.text = @"已读";
            cell.stateBtn.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            cell.stateBtn.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1].CGColor;
            cell.stateBtn.backgroundColor = [UIColor whiteColor];
        });
    }
    
    PartyArticle *model = self.modelArray[indexPath.row];
    PartyArticleDetailVC *detailVC = [[PartyArticleDetailVC alloc] init];
    detailVC.nId = model.nId;
    detailVC.type = model.vcType;
    detailVC.isDisPlayBtm = NO;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - lazy
- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kTopHeight - 44 - kBottomSafeAreaHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.page = 1;
            [weakSelf requestForSaleGongdanList];
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            self.page ++;
            [weakSelf requestForSaleGongdanList];
        }];
    }
    
    return _tableView;
}

- (NSMutableArray *)modelArray{
    if (_modelArray == nil) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
