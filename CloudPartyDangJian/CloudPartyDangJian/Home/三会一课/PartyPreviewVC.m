//
//  PartyPreviewVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "PartyPreviewVC.h"
#import <QuickLook/QuickLook.h>

@interface PartyPreviewVC ()<QLPreviewControllerDataSource>

@end

@implementation PartyPreviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configViews];
    self.title = @"文档浏览";
    
    // Do any additional setup after loading the view.
}

- (void)configViews{
    // 将QLPreviewControler  添加到本控制器上
    QLPreviewController *HPQLController = [[QLPreviewController alloc] init];
    HPQLController.dataSource = self;
    [self addChildViewController:HPQLController];
    [HPQLController didMoveToParentViewController:self];
    [self.view addSubview:HPQLController.view];
    HPQLController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - kTopHeight);
    
    //刷新界面,如果不刷新的话，不重新走一遍代理方法，返回的url还是上一次的url
    [HPQLController refreshCurrentPreviewItem];
}

#pragma mark - QLPreviewControllerDataSource
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller{
    return 1;
}

- (id<QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index{
    
    return self.url;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
