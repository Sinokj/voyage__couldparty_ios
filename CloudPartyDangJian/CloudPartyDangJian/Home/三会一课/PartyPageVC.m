//
//  PartyPageVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "PartyPageVC.h"

@interface PartyPageVC ()

@end

@implementation PartyPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)initViewControllers{
    NSMutableArray *controllers = [NSMutableArray array];
    for (int i = 0; i < self.channels.count; i ++) {
        NSString *name = self.channels[i];
        PartySubVC *subVC = [PartySubVC new];
        subVC.yp_tabItemTitle = name;
        [controllers addObject:subVC];
    }
    self.viewControllers = controllers;
}

- (void)setChannels:(NSArray *)channels{
    _channels = channels;
    
    [self setTabBarFrame:CGRectMake(0, 0, ScreenWidth, 44)
        contentViewFrame:CGRectMake(0, 44, ScreenWidth, ScreenHeight - kTopHeight  - 44 - kBottomSafeAreaHeight)];
    self.tabBar.itemTitleColor = [UIColor lightGrayColor];
    self.tabBar.itemTitleSelectedColor = AppTintColor;
    self.tabBar.itemTitleFont = [UIFont systemFontOfSize:15];
    self.tabBar.itemTitleSelectedFont = [UIFont boldSystemFontOfSize:15];
    
    self.tabBar.indicatorScrollFollowContent = YES;
    self.tabBar.indicatorColor = AppTintColor;
    
    self.tabBar.leadingSpace = 15;
    self.tabBar.trailingSpace = 15;
    [self.tabBar setIndicatorWidth:40 marginTop:40 marginBottom:0 tapSwitchAnimated:NO];
    [self.tabBar setScrollEnabledAndItemFitTextWidthWithSpacing:30 * kWScale];
    
    
    [self.tabContentView setContentScrollEnabled:YES tapSwitchAnimated:NO];
    self.tabContentView.loadViewOfChildContollerWhileAppear = YES;
    [self initViewControllers];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
