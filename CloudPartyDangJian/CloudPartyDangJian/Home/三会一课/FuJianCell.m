//
//  FuJianCell.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "FuJianCell.h"
#import "MJProgressView.h"
#import "MJDownload.h"
#import "STNavigationController.h"
#import "PartyPreviewVC.h"

@interface FuJianCell()
@property (nonatomic, strong)UILabel *label;
@property (nonatomic, strong)UIButton *loadBtn;
@property (nonatomic, strong)UIButton *openBtn;
@property (nonatomic, strong)MJProgressView *progressView;
@end

@implementation FuJianCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self drawCell];
    }
    return self;
}

- (void)setModel:(FuJianModel *)model{
    _model = model;
    self.label.text = model.vcName;
    
    //获取url信息
    NSString *url = model.vcUrl;
    MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:url];
    if (info.state == MJDownloadStateCompleted) {//已经完全下载完毕
        self.openBtn.hidden = NO;
        self.loadBtn.hidden = YES;
        self.progressView.hidden = YES;
    } else if (info.state == MJDownloadStateWillResume) {//即将下载（等待下载）
        self.openBtn.hidden = YES;
        self.loadBtn.hidden = NO;
        self.progressView.hidden = NO;
        [self.loadBtn setTitle:@"等待" forState:UIControlStateNormal];
    } else {
        self.openBtn.hidden = YES;
        self.loadBtn.hidden = NO;
        
        if (info.state == MJDownloadStateNone ) {//闲置状态（除后面几种状态以外的其他状态）
            self.progressView.hidden = YES;
        } else {
            self.progressView.hidden = NO;
            
            if (info.totalBytesExpectedToWrite) {
                self.progressView.progress = 1.0 * info.totalBytesWritten / info.totalBytesExpectedToWrite;
            } else {
                self.progressView.progress = 0.0;
            }
        }
        
        if (info.state == MJDownloadStateResumed) {
            [self.loadBtn setTitle:@"暂停" forState:UIControlStateNormal];
        } else {
            [self.loadBtn setTitle:@"下载" forState:UIControlStateNormal];
        }
    }
}

- (void)drawCell{
    UIImageView *icon = [UIImageView new];
    icon.image = [UIImage imageNamed:@"partyFuJian"];
    [self.contentView addSubview:icon];
    
    icon.sd_layout
    .leftSpaceToView(self.contentView, 12)
    .centerYEqualToView(self.contentView)
    .widthIs(22)
    .heightIs(23);
    
    self.loadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loadBtn.backgroundColor = AppTintColor;
    self.loadBtn.hidden = NO;
    [self.loadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loadBtn setTitle:@"下载" forState:UIControlStateNormal];
    self.loadBtn.titleLabel.font = FontSystem(14);
    [self.loadBtn addTarget:self action:@selector(actionForDownLoad) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.loadBtn];
    
    self.loadBtn.sd_layout
    .rightSpaceToView(self.contentView, 12)
    .widthIs(52)
    .heightIs(24)
    .centerYEqualToView(self.contentView);
    self.loadBtn.sd_cornerRadius = @(4);

    self.openBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openBtn.backgroundColor = AppTintColor;
    self.openBtn.hidden = YES;
    [self.openBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.openBtn setTitle:@"打开" forState:UIControlStateNormal];
    self.openBtn.titleLabel.font = FontSystem(14);
    [self.openBtn addTarget:self action:@selector(actionForOpenClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.openBtn];
    
    self.openBtn.sd_layout
    .rightSpaceToView(self.contentView, 12)
    .widthIs(52)
    .heightIs(24)
    .centerYEqualToView(self.contentView);
    self.openBtn.sd_cornerRadius = @(4);
    
    self.label = [[UILabel alloc] init];
    self.label.textColor = UIColorHex(3A3A3A);
    self.label.font = FontBold(15);
    [self.contentView addSubview:self.label];
    
    self.label.sd_layout
    .leftSpaceToView(icon, 10)
    .centerYEqualToView(icon)
    .rightSpaceToView(self.contentView, 64)
    .heightIs(30);
    
    self.progressView = [[MJProgressView alloc] init];
    self.progressView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.progressView];
    
    self.progressView.sd_layout
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .heightIs(1)
    .bottomSpaceToView(self.contentView, 0);

}

#pragma mark - action
//下载
- (void)actionForDownLoad{
    MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:self.model.vcUrl];
    
    if (info.state == MJDownloadStateResumed || info.state == MJDownloadStateWillResume) {
        [[MJDownloadManager defaultManager] suspend:info.url];
    } else {
        [[MJDownloadManager defaultManager] download:self.model.vcUrl progress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.model = self.model;
            });
        } state:^(MJDownloadState state, NSString *file, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.model = self.model;
            });
        }];
    }
}

//打开
- (void)actionForOpenClick{
    MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:self.model.vcUrl];
    
    //获取导航
    UINavigationController *nav = [self getCurrentNavigationController];
    PartyPreviewVC *previewVC =  [[PartyPreviewVC alloc]  init];
    previewVC.url = [NSURL fileURLWithPath:info.file];
    [nav pushViewController:previewVC animated:YES];
    
}

- (UINavigationController *)getCurrentNavigationController{
    UITabBarController *tabVC = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    STNavigationController *nav = (STNavigationController *)tabVC.selectedViewController;
    return nav;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
