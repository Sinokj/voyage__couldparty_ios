//
//  ALReasonVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "ALReasonVC.h"
#import "QMtextView.h"
#import "ALMsgApplyDetailVC.h"

@interface ALReasonVC ()
/** 输入框 */
@property (nonatomic, strong)QMtextView *textView;
@end

@implementation ALReasonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    //导航条
    if ([self.type isEqualToString:@"请假"]) {
        self.title = @"请假原因";
    }else if([self.type isEqualToString:@""]){
        self.title = @"请假原因";
    }
    
    //绘制页面
    [self drawSubView];
    
}

- (void)drawSubView{
    
    self.textView = [QMtextView new];
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.placeholder = @"请输入请假原因";
    self.textView.font = FontSystem(17);
    [self.view addSubview:self.textView];
    
    self.textView.sd_layout
    .leftSpaceToView(self.view, 12)
    .rightSpaceToView(self.view, 12)
    .topSpaceToView(self.view, 10)
    .heightIs(220 * kWScale);
    
    UIButton *commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    commitBtn.titleLabel.font = FontSystem(18);
    [commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    commitBtn.backgroundColor = AppTintColor;
    [commitBtn setTitle:@"提   交" forState:UIControlStateNormal];
    [commitBtn addTarget:self action:@selector(actionForCommit) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:commitBtn];
    
    commitBtn.sd_layout
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(self.view, kBottomSafeAreaHeight)
    .heightIs(100 * kWScale);
}


- (void)actionForCommit{
    if (self.textView.text.length == 0) {
        [SMGlobalMethod showMiddleMessage:@"请填写描述"];
        return;
    }
    
    if ([self.type isEqualToString:@"请假"]) {
        [DECommenAlert showAlertWithContent:@"确定请假吗?" leftBtn:@"取消" rightBtn:@"确定" tapEvent:^(NSInteger index) {
            [DECommenAlert dismiss];
            if (index == 1) {
                NSString *url = [NSString stringWithFormat:@"%@nId=%ld&vcReason=%@",SanHuiYiKeLeave, self.nId,self.textView.text];
                [DataRequest GetWithURL:url :^(id reponserObject) {
                    for (UIViewController *vc in self.navigationController.viewControllers) {
                        if ([vc isKindOfClass:[ALMsgApplyDetailVC class]]) {
                            ALMsgApplyDetailVC *detailVC = (ALMsgApplyDetailVC *)vc;
                            if (detailVC.popBlock) {
                                detailVC.popBlock();
                            }
                            [self.navigationController popToViewController:vc animated:YES];
                        }
                    }
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    [SMGlobalMethod showMiddleMessage:reponserObject[@"vcResult"]];
                }];
            }
        }];
    }else{
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
