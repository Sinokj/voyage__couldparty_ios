//
//  HomeController.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "ViewController.h"

@interface HomeController : ViewController

/**
 登录之后要更新数据

 @param partyIDStr 党组的id
 */
- (void)updateInfoAfterLoginIn:(NSString *)partyIDStr;
@end
