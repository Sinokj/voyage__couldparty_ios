//
//  ListCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ListCell.h"
#import "DyfTool.h"
#import <UIImageView+AFNetworking.h>
@interface ListCell ()

//@property (nonatomic, strong) UIImageView *icoImageV;

@property (weak, nonatomic) IBOutlet UIImageView *icoImageV;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//@property (nonatomic, strong) UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

//@property (nonatomic, strong) UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

//@property (nonatomic, strong) UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *seeLabel;

//@property (nonatomic, strong) UILabel *seeLabel;
@property (weak, nonatomic) IBOutlet UILabel *alertL;

//@property (nonatomic, strong) UILabel *alertL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthContant;

//@property (nonatomic, strong) UIImageView *seeImgv;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertLWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertLHeightConstraint;


@end
@implementation ListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.timeLabel sizeToFit];
        [self.seeLabel sizeToFit];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
//        [self prepareLayout];
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
    self.alertL.layer.cornerRadius = MYDIMESCALEW(8);
    self.alertL.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
    self.alertL.clipsToBounds = YES;
    self.imageWidthContant.constant = MYDIMESCALEW(107);
    self.alertLWidthConstraint.constant = MYDIMESCALEW(45);
    self.alertLHeightConstraint.constant = MYDIMESCALEW(15);
    [STUtils setupView:self.icoImageV cornerRadius:5 bgColor:nil borderW:0 borderColor:nil];
}

- (void)prepareLayout {
    
    self.contentView.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, ScreenWidth - 6, 103)];
    [self.contentView addSubview:bgView];
    bgView.backgroundColor = [UIColor whiteColor];
    
//    self.icoImageV = [[UIImageView alloc] initWithFrame:CGRectMake(bgView.left + 5, bgView.top + 5, 100, bgView.height - 25)];
    
    self.icoImageV.backgroundColor = [UIColor orangeColor];
    [bgView addSubview:self.icoImageV];
    
    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(self.icoImageV.right + 8, self.icoImageV.top + 5, bgView.width - self.icoImageV.width - 15, 20) andTitle:@"测试" andFontNumber:15];
    [bgView addSubview:self.titleLabel];
    
    
//    self.detailLabel = [UILabel customLablWithFrame:CGRectMake(self.titleLabel.left, self.titleLabel.bottom, self.titleLabel.width, 40) andTitle:@"什么打扫没打算模式摸摸发哦摸吗佛莫萨摩佛山吗" andFontNumber:13];
//    self.detailLabel.textColor =  COLOR_RGB(0x808080);
//    self.detailLabel.numberOfLines = 0;
//    [bgView addSubview:self.detailLabel];
    
    UIImageView *timeIco = [[UIImageView alloc] initWithFrame:CGRectMake(self.icoImageV.left, self.icoImageV.bottom + 8, 10, 10)];
    timeIco.image = [UIImage imageNamed:@"4-Time"];
    [bgView addSubview:timeIco];
    
    self.timeLabel = [UILabel customLablWithFrame:CGRectMake(timeIco.right, timeIco.top, self.icoImageV.width + 40, 20) andTitle:@"2016-11-11 22:11" andFontNumber:12];
    self.timeLabel.textColor =  COLOR_RGB(0x808080);
    self.timeLabel.centerY = timeIco.centerY;
    [bgView addSubview:self.timeLabel];
    
    
    self.seeLabel = [UILabel customLablWithFrame:CGRectMake(ScreenWidth - 100, self.timeLabel.top, 50, 20) andTitle:@"浏览:99999" andFontNumber:12];
    [bgView addSubview:self.seeLabel];
    
    self.seeLabel.textColor = COLOR_RGB(0x808080);
    self.seeLabel.textAlignment = NSTextAlignmentLeft;
    self.seeLabel.centerY = self.timeLabel.centerY;
    
    UIImageView *seeIco = [[UIImageView alloc] initWithFrame:CGRectMake(self.seeLabel.left - 15, self.seeLabel.top, 15, 15)];
//    self.seeImgv = seeIco;
    seeIco.centerY = self.seeLabel.centerY;
    seeIco.image = [UIImage imageNamed:@"read"];
    [bgView addSubview:seeIco];
    
    self.alertL = [UILabel customLablWithFrame:CGRectMake(_seeLabel.right, self.timeLabel.top, 50, 20) andTitle:@"" andFontNumber:12];
    self.alertL.centerY = self.seeLabel.centerY;
    self.alertL.textColor = [UIColor redColor];
    [bgView addSubview:self.alertL];
    
    
    
    
}

- (void)setModel:(NewList *)model {
    if (_model != model) {
        _model = nil;
        _model = model;
        [self.icoImageV setImageWithURL:[NSURL URLWithString:model.vcPath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
//        [self.icoImageV sd_setImageWithURL:[NSURL URLWithString:model.vcPath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        self.titleLabel.text = model.vcTitle;
        self.detailLabel.text = model.vcDescribe;
        self.timeLabel.text = [NSString stringWithFormat:@"%@",[model.dtReg substringToIndex:model.dtReg.length - 3]];
        self.seeLabel.text = [NSString stringWithFormat:@"%ld阅读",(long)model.nclick];
        
        if (self.isLearn) {
            
//            self.seeImgv.hidden = YES;
//            self.seeLabel.text = @"已学习";
        }
        
//        self.alertL.hidden = [STUtils objectForKey:KDangJianType] && [[STUtils objectForKey:KDangJianType] integerValue] == 2;
        
        if (model.nStatus > 0) {
            
            self.alertL.text = @"已读";
         
            self.alertL.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            self.alertL.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1].CGColor;
            self.alertL.backgroundColor = [UIColor whiteColor];
            
        }else if (model.nStatus == 0) {
            
            self.alertL.text = @"未读";
            self.alertL.textColor = AppTintColor;
            self.alertL.layer.borderColor = AppTintColor.CGColor;
            self.alertL.backgroundColor = [UIColor whiteColor];
            
            
        }
    }
    
    
    // 没有登录也隐藏
    if ([STUtils isLogin]) {
        self.alertL.hidden = NO;
        self.timeLeftMarginConstraint.priority = 999;
        self.timeLeftToSuperViewConstraint.priority = 750;
    }else {
        self.alertL.hidden = YES;
        self.timeLeftMarginConstraint.priority = 750;
        self.timeLeftToSuperViewConstraint.priority = 999;
    }
}

- (void)setIsShow:(BOOL)isShow {
    _isShow = isShow;
    
    if (isShow) {
//        self.alertL.text = @"";
    }
}


@end
