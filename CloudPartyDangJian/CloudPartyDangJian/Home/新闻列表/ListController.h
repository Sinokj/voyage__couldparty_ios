//
//  ListController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "ListModel.h"

@interface ListController : ViewController

@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) ListModel *listM;

@property (nonatomic, assign) NSInteger nModuleId;//模块Id

@end
