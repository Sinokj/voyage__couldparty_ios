//
//  AnswerCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "AnswerCell.h"
#import "DyfTool.h"
#import <Masonry.h>

@interface AnswerCell ()
@property (nonatomic, strong) UIButton *checkBtn; //勾选按钮
@end
@implementation AnswerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {

    
    self.choseBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    
//    self.choseBtn.frame = CGRectMake(10, 14, 25, 25);
    
//    [self.choseBtn setImage:[UIImage imageNamed:@"login_ed"] forState:(UIControlStateNormal)];
//    [self.choseBtn setImage:[UIImage imageNamed:@"login_btn"] forState:(UIControlStateSelected)];
//    [self.choseBtn setBackgroundImage:ImageName(@"login_ed") forState:(UIControlStateNormal)];
//    [self.choseBtn setBackgroundImage:ImageName(@"login_btn") forState:(UIControlStateSelected)];
    [self.choseBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.choseBtn setTitleColor:AppTintColor forState:(UIControlStateSelected)];
//    self.choseBtn.enabled = NO;
    self.choseBtn.titleLabel.numberOfLines = 0;
    self.choseBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.choseBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 45, 0, 10);
    self.choseBtn.titleLabel.font = [UIFont systemFontOfSize:15.0];
    
    [self.contentView addSubview:self.choseBtn];
    
    [self.choseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.offset(0);
        make.left.offset(15);
        make.top.offset(10);
        make.size.mas_equalTo(CGSizeMake(MAIN_SIZE.width - 54, 40));
    }];
    
    UIView *lineV = [UIView new];
    lineV.backgroundColor = UIColorHex(949494);
    [self.contentView addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.choseBtn).offset(8);
        make.right.equalTo(self.choseBtn).offset(-8);
        make.bottom.equalTo(self.choseBtn);
        make.height.mas_equalTo(1/YYScreenScale());
    }];
    
    [self.choseBtn resignFirstResponder];
    self.choseBtn.userInteractionEnabled = NO;
    
    self.checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"icon_dorpbox_nor"] forState:UIControlStateNormal];
    [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"icon_dorpbox_pre"] forState:UIControlStateSelected];
    [self.contentView addSubview:self.checkBtn];
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.choseBtn.mas_left).offset(8);
        make.centerY.equalTo(self.choseBtn.mas_centerY);
        make.width.height.mas_equalTo(25);
    }];

    
    
    
//    self.contentL = [UILabel customLablWithFrame:CGRectZero andTitle:@"" andFontNumber:15];
//    self.contentL.numberOfLines = 0;
//    [self.contentView addSubview:self.contentL];
//    [self.contentL mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.offset(0);
//        make.left.equalTo(_choseBtn.mas_right).offset(13);
//    }];
    
}

- (void)setChoicesM:(Choices *)choicesM {
    if (_choicesM != choicesM) {
        _choicesM = nil;
        _choicesM = choicesM;
        
//        self.contentL.text = choicesM.vcAnswer;
        [self.choseBtn setTitle:choicesM.vcAnswer forState:UIControlStateNormal];
        
//        CGFloat height = [self stringHeightWithStr:choicesM.vcAnswer andSize:15 maxWidth:ScreenWidth - 60];
        
//        self.contentL.height = height;
        
//        self.choseBtn.centerY = self.contentL.centerY;
        
    }
    
    self.choseBtn.selected = choicesM.isSelect;
    self.checkBtn.selected = self.choseBtn.selected;
    [self setNeedsDisplay];
}

- (float)stringHeightWithStr:(NSString *)text andSize:(CGFloat)fontSize maxWidth:(CGFloat)maxWidth {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:fontSize],NSFontAttributeName, nil];
    
    float height = [text boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    return ceilf(height);
}

- (void)selectBtnClickWithRow:(NSInteger)row andModel:(Exam *)exam andType:(BOOL)type  andFirst:(BOOL)first block:(void(^)(void))block {
    if (!type) {
        for (Choices *choice in exam.choices) {
            choice.isSelect = NO;
        }
    }
    if (first) {
        for (Choices *choice in exam.choices) {
            choice.isSelect = NO;
        }

    }
    
    exam.choices[row].isSelect = !exam.choices[row].isSelect;
    
    block();
    
}

+ (CGFloat)CellheightWithStr:(NSString *)content {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:15],NSFontAttributeName, nil];
    
    float height = [content boundingRectWithSize:CGSizeMake(ScreenWidth - 60, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    if (ceilf(height) > 50) {
        return ceilf(height) + 10;
    }else {
        return 50;
    }
    

}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    //设置虚线颜色
    CGContextSetStrokeColorWithColor(currentContext, [UIColor colorWithWhite:1 alpha:1].CGColor);
    //设置虚线宽度
    CGContextSetLineWidth(currentContext, 1);
    //设置虚线绘制起点
    CGContextMoveToPoint(currentContext, 10, [AnswerCell CellheightWithStr:self.choicesM.vcAnswer] - 1);
    //设置虚线绘制终点
    CGContextAddLineToPoint(currentContext, self.frame.size.width, [AnswerCell CellheightWithStr:self.choicesM.vcAnswer] - 1);
    //设置虚线排列的宽度间隔:下面的arr中的数字表示先绘制3个点再绘制1个点
    CGFloat arr[] = {3,2};
    //下面最后一个参数“2”代表排列的个数。
    CGContextSetLineDash(currentContext, 0, arr, 2);
    CGContextDrawPath(currentContext, kCGPathStroke);
}

@end
