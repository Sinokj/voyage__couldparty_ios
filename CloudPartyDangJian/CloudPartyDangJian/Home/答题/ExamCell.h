//
//  ExamCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExamsModel.h"

@interface ExamCell : UICollectionViewCell

@property (nonatomic, strong) Exam *examModel;

@property (nonatomic, assign) BOOL type;

@property (nonatomic, assign) BOOL isOver;

@property (nonatomic, strong) NSArray *answer;

@property (nonatomic, copy) void (^ExamBlock)(NSDictionary *dic, NSString *key);

@property (nonatomic, strong) UIButton *indexBtn;

@end
