//
//  HomeModel.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Homemodel;
@interface HomeModel : NSObject

@property (nonatomic, strong) NSArray<Homemodel *> *objects;

@end

@interface Homemodel : NSObject

@property (nonatomic, assign) NSInteger unRead;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *vcName;

@end

