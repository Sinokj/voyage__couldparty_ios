//
//  VoteListController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "VoteModels.h"

@interface VoteListController : ViewController

@property (nonatomic, strong) VoteModels *voteModels;

@property (nonatomic, assign) BOOL isFirst;
/**
 *  yes 只显示投票内容
 */
@property (nonatomic, assign) BOOL isAll;

@end
