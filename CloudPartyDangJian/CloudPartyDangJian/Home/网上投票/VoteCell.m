//
//  VoteCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "VoteCell.h"
#import "DyfTool.h"

@interface VoteCell ()

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UILabel *stateLabel;

@end
@implementation VoteCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.contentView.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, ScreenWidth - 6, 80)];
    
    [self.contentView addSubview:bgView];
    
    bgView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
    
    imageV.image = [UIImage imageNamed:@"ic_doc"];
    
    [bgView addSubview:imageV];
    
    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(imageV.right + 10, 5, bgView.width - 60, 20) andTitle:@"测试" andFontNumber:15];
    
    [bgView addSubview:self.titleLabel];
    
    self.timeLabel = [UILabel customLablWithFrame:CGRectMake(10, imageV.bottom + 8, self.titleLabel.width, 20) andTitle:@"到期时间:2016-11-11 22:11" andFontNumber:14];
    
    [bgView addSubview:self.timeLabel];
    
    self.stateLabel = [UILabel customLablWithFrame:CGRectMake(ScreenWidth - 100, self.titleLabel.bottom, 80, 20) andTitle:@"" andFontNumber:14];
    
    self.stateLabel.textAlignment = NSTextAlignmentRight;
    
    self.stateLabel.textColor = COLOR_RGB(0xffd700);
    
    [bgView addSubview:self.stateLabel];
    
}
- (void)setTitle:(NSString *)title {
    if (_title != title) {
        _title = nil;
        _title = title;
        
        self.titleLabel.text = title;
    }
}

- (void)setEndTime:(NSString *)endTime {
    if (_endTime != endTime) {
        _endTime = nil;
        _endTime = endTime;
        
        self.timeLabel.text = [NSString stringWithFormat:@"到期时间: %@",endTime];
    }
}

- (void)setIsVoted:(NSInteger)isVoted {
    if (_isVoted != isVoted) {
        
        _isVoted = isVoted;
        
        if (isVoted != 0) {
            self.stateLabel.text = @"已参加";
        }else {
            self.stateLabel.text = @"";
        }
    }
}

- (void)setTitle:(NSString *)title andBeginTime:(NSString *)begin andEndTime:(NSString *)end andState:(NSInteger)state {
    
    self.titleLabel.text = title;
    
    self.timeLabel.text = [NSString stringWithFormat:@"%@至%@",[begin substringToIndex:begin.length - 3],[end substringToIndex:end.length - 3]];
    
    if (state == 1) {
        self.stateLabel.text = @"已结束";
    }else {
        self.stateLabel.text = @"";
    }
}
@end
