//
//  PayMoneyViewController.m
//  EPartyConstruction
//
//  Created by df on 2017/5/10.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "PayMoneyViewController.h"
//#import <AlipaySDK/AlipaySDK.h>
//#import "PayManager.h"

#import "SuccessViewController.h"
#define TRUEW(v) (v*([UIScreen mainScreen].bounds.size.width)/320)

@interface PayMoneyViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textF;

@property (weak, nonatomic) IBOutlet UIImageView *alipaySelect;

@property (weak, nonatomic) IBOutlet UIImageView *weixinSelect;

@property (nonatomic, assign) BOOL selectAlipay;

@property (nonatomic, assign) NSInteger nOrderId;

@property (weak, nonatomic) IBOutlet UILabel *dateTF;

@property (weak, nonatomic) IBOutlet UILabel *superTF;
@property (weak, nonatomic) IBOutlet UILabel *nameTF;

@property (nonatomic, strong) UIButton *superBtn;


@end

@implementation PayMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"党费交纳";
    
    self.selectAlipay = YES;

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月"];
    
    self.dateTF.text = [formatter stringFromDate:[NSDate date]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"支付" style:(UIBarButtonItemStylePlain) target:self action:@selector(payClick)];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
//    [_btnCell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, MAXFLOAT)];
    

    
    self.superTF.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"partyGroupName"];
    



    self.nameTF.text =  [[NSUserDefaults standardUserDefaults] objectForKey:@"vcName"];

    
    
    self.textF.keyboardType = UIKeyboardTypeDecimalPad;
    
    self.textF.delegate = self;
    
}

-(UIButton*)superBtn
{
    if (_superBtn==nil) {
        
        _superBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _superBtn.frame = CGRectMake(0, TRUEW(2), TRUEW(15), TRUEW(20));
        
        [_superBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        
        [_superBtn addTarget:self action:@selector(superBtnClick) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return _superBtn;
}
-(void)superBtnClick {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section == 2 ? 30 : 15;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2) {
        
        if ( indexPath.row == 0) {
            [self weixinClick];
        }else {
            [self alipayClick];
        }
    }
    
    
}


- (void)alipayClick {
    
    self.selectAlipay = YES;
    
    self.weixinSelect.image = nil;
    
    self.alipaySelect.image = [UIImage imageNamed:@"icon_choiced"];
}

- (void)weixinClick {
    
    self.selectAlipay = NO;
    
    self.weixinSelect.image = [UIImage imageNamed:@"icon_choiced"];
    
    self.alipaySelect.image = nil;
}

- (void)payClick {
    
    if ([self.textF.text floatValue] <= 0 || self.textF.text.length == 0) {
        
        [MBProgressHUD showMessage:@"金额不正确" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        
        return;
    }
    
#pragma mark - 支付宝
    
    if (self.selectAlipay) {
    
        [DataRequest PostWithURL:PartyAliPayCheck parameter:@{@"money": self.textF.text, @"vcSource": @"iOS"} :^(id reponserObject) {
            
            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
            
            [user setObject:self.textF.text forKey:@"payMoney"];
            
            NSString* resultStr = [NSString stringWithFormat:@"%@",reponserObject[@"nRes"]];
            if ([resultStr isEqualToString:@"1"]) {
//                [self AlipayNow:reponserObject[@"payInfo"]];
            }else {
                
                [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:2 ToView:self.view userInteractionEnabled:YES];
            }
            
        }];
        
    }else {
        
        [DataRequest PostWithURL:WXPay parameter:@{@"money": self.textF.text, @"vcSource": @"iOS"} :^(id reponserObject) {
            
            
            NSDictionary *dic = reponserObject;
            
            if ([reponserObject[@"nRes"] integerValue] == 1) {
                
                self.nOrderId = [reponserObject[@"nOrderId"] integerValue];
                
//                [[PayManager sharedManager] wxPayWithPartnerId:dic[@"payInfo"][@"partnerid"] andPrepayId:dic[@"payInfo"][@"prepayid"] andNonceStr:dic[@"payInfo"][@"noncestr"] andTimeStamp:dic[@"payInfo"][@"timestamp"] andSign:dic[@"payInfo"][@"sign"]];
//                [PayManager sharedManager].PayManagerBlock = ^(enum WXErrCode isPayOver) {
//
//                    switch (isPayOver) {
//
//                        case WXSuccess:
//
//                            [self weixinPayResult];
//
//                            break;
//
//                        case WXErrCodeCommon:
//
//                            [MBProgressHUD showMessage:@"网络超时,请稍后再试" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
//
//                            break;
//
//                        case WXErrCodeUserCancel:
//
//                            [MBProgressHUD showMessage:@"取消支付" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
//
//                            break;
//
//                        case WXErrCodeSentFail:
//
//                            [MBProgressHUD showMessage:@"发送失败" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
//
//                            break;
//
//                        case WXErrCodeAuthDeny:
//
//                            [MBProgressHUD showMessage:@"授权失败" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
//
//                            break;
//
//                        default:
//
//                            [self weixinPayResult];
//
//                            break;
//                    }
//
//                };
//
            }else {
                
                 [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:2 ToView:self.view userInteractionEnabled:YES];
            }
            
        }];
        
        
        
    }
    
    
}

#pragma mark - 微信支付

- (void)weixinPayResult {
    
    [DataRequest PostWithURL:WXGetPayResult parameter:@{@"nOrderId": @(self.nOrderId)} :^(id reponserObject) {
       
        if ([reponserObject[@"nRes"] integerValue] == 1) {
            
            SuccessViewController *success = [SuccessViewController new];
            
            success.money = self.textF.text;
            
            [self.navigationController pushViewController:success animated:YES];
        }
        
    }];
}

#pragma - mark - 支付宝
//-(void)AlipayNow:(NSString*)payInfo{
//    //支付回调
//    [[AlipaySDK defaultService] payOrder:payInfo fromScheme:@"EPartyConstruction" callback:^(NSDictionary *resultDic){
//
//        NSString* resultStr = [NSString stringWithFormat:@"%@",resultDic[@"resultStatus"]];
//        if ([resultStr isEqualToString:@"9000"]){//支付成功
//
//            [self.navigationController popToRootViewControllerAnimated:YES];
//
//        }
//    }];
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([str integerValue] > 1000) {
        
        textField.text = @"1000";
        
        return NO;
    }
    return YES;
}

- (UITextField *)customWithFrame:(CGRect)frame andLeftTitle:(NSString *)leftT andPlaceH:(NSString *)placeho {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 120, frame.size.height - 20)];
    label.text = leftT;
    label.textColor = [UIColor colorWithRed:0.4353 green:0.4353 blue:0.4353 alpha:1.0];
    label.font = [UIFont systemFontOfSize:15];
    UITextField *textF = [[UITextField alloc] initWithFrame:frame];
    textF.leftViewMode = UITextFieldViewModeAlways;
    textF.layer.borderWidth = 0;
    textF.leftView = label;
    textF.placeholder = placeho;
    textF.clearButtonMode = UITextFieldViewModeWhileEditing;
    textF.font = [UIFont systemFontOfSize:13];
    textF.backgroundColor = [UIColor whiteColor];
//    textF.borderStyle = UITextBorderStyleRoundedRect;
    return textF;
}


@end
