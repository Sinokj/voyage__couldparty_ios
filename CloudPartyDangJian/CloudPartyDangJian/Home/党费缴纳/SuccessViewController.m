//
//  SuccessViewController.m
//  EPartyConstruction
//
//  Created by df on 2017/5/10.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "SuccessViewController.h"

@interface SuccessViewController ()

@end

@implementation SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"结果";
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    self.view.backgroundColor = [UIColor whiteColor];

    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:self.view.frame];
    
    bgView.image = [UIImage imageNamed:@"start"];
    
    [self.view addSubview:bgView];
    
    UILabel *scoreL = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, ScreenWidth - 20, 40)];
    
    scoreL.font = FontSystem(16);
    
    [bgView addSubview:scoreL];
    
    scoreL.textAlignment = NSTextAlignmentCenter;
    
    scoreL.text = [NSString stringWithFormat:@"已成功交纳金额  :    %@  元",self.money];


}



-(void)superBtnClick  {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
