//
//  AppDelegate.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/19.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "AppDelegate.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>

//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>

//微信SDK头文件
#import "WXApi.h"


// 引入JPush功能所需头文件
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
// 如果需要使用idfa功能所需要引入的头文件（可选）
//#import <AdSupport/AdSupport.h>
#if DEBUG
#import <FLEXManager.h>
#endif
#import "CheckVersionTool.h"

//通知详情
#import "ALMyMsgModel.h"
#import "STNavigationController.h"
#import "ALMsgApplyDetailVC.h"
#import "PartyArticleListModel.h"
#import "PartyArticleDetailVC.h"

@interface AppDelegate ()<JPUSHRegisterDelegate>

@end

@implementation AppDelegate


static void extracted(AppDelegate *object) {
    [object setupTabBar];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [MyDimeScale setUITemplateSize:CGSizeMake(375, 667)];
    
    //设置cookie
    [STUtils setupCookie];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor redColor];
    TabbarController *tabBarVc = [[TabbarController alloc] init];
    self.tabBarVc = tabBarVc;
    self.window.rootViewController = tabBarVc;
     [[UITabBar appearance] setTranslucent:NO];
    
    // 未登录的党委ID默认是2
//    [STUtils setObject:@2 forKey:KDangJianType];
    
    // 网络监测
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    extracted(self);
    [self setupShareFunction];
    [self setupJPushFunctionWithLaunchOptions:launchOptions];
    
    // 启动图
    UIViewController *lanuchVc = [[UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil] instantiateViewControllerWithIdentifier:@"LanuchScreen"];
    self.lanuchView = lanuchVc.view;
    
    self.lanuchImageView = [[UIImageView alloc] initWithFrame:lanuchVc.view.bounds];
    [self.lanuchView addSubview:self.lanuchImageView];
    
    [DataRequest GetWithURL:StartPhoto :^(id reponserObject) {
        if (reponserObject) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.lanuchImageView sd_setImageWithURL:[NSURL URLWithString:reponserObject[@"vcIcon"]] placeholderImage:nil];
            });
        }
    }];
    
    //检查版本工薪
    [CheckVersionTool checkVersionToUpdate];
    
    [self.window addSubview:self.lanuchView];
    [self.window bringSubviewToFront:self.lanuchView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1 animations:^{
            self.lanuchView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [self.lanuchImageView removeFromSuperview];
            self.lanuchImageView = nil;
            [self.lanuchView removeFromSuperview];
            self.lanuchView = nil;
        }];
    });
    return YES;
}


-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    if (self.allowRotation) {
        return UIInterfaceOrientationMaskAll;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}


//设置底部标签栏
- (void)setupTabBar {
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName: AppTintColor,
                                                        NSFontAttributeName: FontSystem(MYDIMESCALEW(12))
                                                        } forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                                        NSFontAttributeName: FontSystem(MYDIMESCALEW(12))
                                                        }
                                             forState:UIControlStateNormal];
    
    NSDictionary *dict = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                           NSFontAttributeName: FontSystem(MYDIMESCALEW(16))
                           };
    [[UINavigationBar appearance] setTitleTextAttributes:dict];
    
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UITextField appearance] setTintColor:AppTintColor];
    [[UITextView appearance] setTintColor:AppTintColor];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
//    [JPUSHService resetBadge];
    //设置显示数字
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}


//推送功能
- (void)setupJPushFunctionWithLaunchOptions:(NSDictionary *)launchOptions {
    
    //Required
    //notice: 3.0.0及以后版本注册可以这样写，也可以继续用之前的注册方式
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:entity.types categories:nil];
        
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
        UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    //配置极光推送
    BOOL isProduction;
#if DEBUG
    isProduction = NO;
#else
    isProduction = YES;
#endif
    [JPUSHService setupWithOption:launchOptions
                           appKey:JiGuangKey
                          channel:@"App Store"
                 apsForProduction:isProduction
            advertisingIdentifier:nil];
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"实现注册APNs失败接口: did Fail To Register For Remote Notifications With Error: %@", error);
}

#pragma mark- JPUSHRegisterDelegate
// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();
    
    
    if (![STUtils isLogin]) {
        return;
    }
    
    //处理自己的事件
//    NSDictionary *aps = [userInfo valueForKey:@"aps"];
//    NSString *content = [aps valueForKey:@"alert"];
    
    ALMyMsgModel *model = [ALMyMsgModel mj_objectWithKeyValues:userInfo];
    NSString *type = model.vcType;
    NSInteger pushNid = [model.nMsgId integerValue];
    
    //解析推送内容
    if ([type isEqualToString:@"审核通知"]) {
        ALMsgApplyDetailVC *detailVC = [ALMsgApplyDetailVC new];
        detailVC.nId = pushNid;
        detailVC.type = type;
        STNavigationController *nav = [[STNavigationController alloc] initWithRootViewController:detailVC];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:NO completion:nil];
        
    }else if ([type isEqualToString:@"请假通知"]) {
        ALMsgApplyDetailVC *detailVC = [ALMsgApplyDetailVC new];
        detailVC.nId = pushNid;
        detailVC.type = type;
        STNavigationController *nav = [[STNavigationController alloc] initWithRootViewController:detailVC];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:NO completion:nil];
        
    }else if ([type isEqualToString:@"会议通知"]) {
        PartyArticleDetailVC *detailVC = [PartyArticleDetailVC new];
        detailVC.nId = pushNid;
        detailVC.type = type;
        STNavigationController *nav = [[STNavigationController alloc] initWithRootViewController:detailVC];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:NO completion:nil];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
    [application setApplicationIconBadgeNumber:[userInfo[@"badge"] integerValue]];
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [application setApplicationIconBadgeNumber:1];
}






//初始化分享功能
- (void)setupShareFunction {
    
    /**初始化ShareSDK应用
     @param activePlatforms
     使用的分享平台集合
     @param importHandler (onImport)
     导入回调处理，当某个平台的功能需要依赖原平台提供的SDK支持时，需要在此方法中对原平台SDK进行导入操作
     @param configurationHandler (onConfiguration)
     配置回调处理，在此方法中根据设置的platformType来填充应用配置信息
     */
    [ShareSDK registerActivePlatforms:@[@(SSDKPlatformTypeWechat),
                                        @(SSDKPlatformTypeQQ)]
                             onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
                 
             default:
                 break;
         }
     }
                      onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
     {
         
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:shareWXAppID
                                       appSecret:shareWXAppSecret];
                 break;
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:shareTencentAppID
                                      appKey:shareTencentAppSecret
                                    authType:SSDKAuthTypeBoth];
                 break;
                 
             default:
                 break;
         }
     }];
    
    
}




@end
