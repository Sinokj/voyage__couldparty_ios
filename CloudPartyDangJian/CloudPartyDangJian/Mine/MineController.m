//
//  MineController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "MineController.h"
#import "MineCell.h"
#import "MineModel.h"
#import "VoteListController.h"
#import "MineReviewedController.h"
#import "MineExamController.h"
#import "MineSetController.h"
#import "MineListController.h"
//#import "CloudPartyDangJian-Swift.h"
#import "MineReviewedScoreController.h"
#import <Masonry.h>
@interface MineController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) UIImageView *icoImageView;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *levelLabel;

@property (nonatomic, strong) UILabel *pointLabel;

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) NSArray *arr;

@property (nonatomic, strong) MineModel *mineModel;

@property (nonatomic, strong) UIImage *photoImageView;

@property (nonatomic, strong) UIView *bgHead;

@property (nonatomic, strong) UIView *textBg;

@property (nonatomic, strong) UIImageView *progressView;

@property (nonatomic, strong) UIImageView *progressBgView;

@property(nonatomic,weak) UINavigationController *navController;

@end

@implementation MineController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self requestData];
    
    //很重要，每次要显示之前都将delegate设置为自己
    self.navigationController.delegate=self;
    _navController=self.navigationController;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"个人中心";
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.9463 green:0.1012 blue:0.1432 alpha:1.0];
    
    self.arr = [NSArray arrayWithObjects:@"我的学习",@"我的答题", @"我的评议", nil];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    [self prepareLayout];
    
}

- (void)requestData {
    
    [DataRequest GetWithURL:MineInfo andIsHome:YES :^(id reponserObject) {
        
        self.mineModel = [MineModel mj_objectWithKeyValues:reponserObject];
        
        [self.icoImageView sd_setImageWithURL:[NSURL URLWithString:self.mineModel.baseInfo.vcHeadImgUrl] placeholderImage:[UIImage imageNamed:@"face"]];
        
        self.nameLabel.text = self.mineModel.baseInfo.vcName;
        
        
        NSString *pointStr = [NSString stringWithFormat:@"%.2f/100",self.mineModel.nLevelPoint];
        
        self.pointLabel.text = pointStr;
        
        //        self.bgHead.width = self.mineModel.nLevelPoint/100.0 * ScreenWidth;
        
        
        
        [_progressView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo((ScreenWidth - 140 - 6) * (self.mineModel.nLevelPoint/100.0));
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [_progressView layoutIfNeeded];
        }];
        
        
        
        
        self.levelLabel.text = self.mineModel.baseInfo.vcLevel;
        
        CGFloat textwidth = [self getWidthWithfont:15 andStr:self.mineModel.baseInfo.vcLevel];
        
        self.levelLabel.width = textwidth;
        
    }];
    
}

-(CGFloat)getWidthWithfont:(CGFloat)f andStr:(NSString *)str {
    
    UIFont * font = [UIFont systemFontOfSize:f];
    CGSize size = [str sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil]];
    CGFloat w = size.width;
    
    return w;
}

//- (void)viewDidDisappear:(BOOL)animated {
//
//    [self.navigationController setNavigationBarHidden:false animated:true];
//
//    [super viewDidDisappear:animated];
//
//
//}


- (void)prepareLayout {
    
    UIView *headBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 300)];
    
    headBgView.backgroundColor = AppTintColor;
    
    UIImageView *backgroundView = [[UIImageView alloc] init];
    
    backgroundView.image = [UIImage imageNamed:@"bg_mine"];
    
    //    backgroundView.frame = CGRectMake(0, 0, ScreenWidth, 300);
    
    [headBgView addSubview:backgroundView];
    
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(headBgView);
    }];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    rightBtn.frame = CGRectMake(0, 2, 20, 20);
    
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal];
    
    [rightBtn addTarget:self action:@selector(setClick) forControlEvents:UIControlEventTouchUpInside];
    
    [headBgView addSubview:rightBtn];
    
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.offset(-10);
        make.top.offset(30);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    // 标题
    UILabel *titleLab = [UILabel customLablWithFrame:CGRectZero andTitle:@"个人中心" andFontNumber:1];
    titleLab.font = [UIFont systemFontOfSize:17];
    
    titleLab.textColor = [UIColor whiteColor];
    [headBgView addSubview:titleLab];
    
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(30);
        make.centerX.offset(0);
    }];
    
    
    
    // 头像背景
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(115, 35, ScreenWidth/4+10, ScreenWidth/4+10)];
    
    bgImg.layer.cornerRadius = 95/2.0;
    
    bgImg.clipsToBounds = YES;
    
    bgImg.centerY = headBgView.centerY - 20;
    
    bgImg.centerX = headBgView.centerX;
    
    bgImg.image = [UIImage imageNamed:@"headBound"];
    
    [headBgView addSubview:bgImg];
    
    [bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(95, 95));
        make.centerX.offset(0);
        make.top.equalTo(titleLab.mas_bottom).offset(25);
    }];
    
    
    
    
    self.icoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(120, 40, ScreenWidth/4, ScreenWidth/4)];
    
    self.icoImageView.layer.cornerRadius = ScreenWidth/4/2;
    
    self.icoImageView.clipsToBounds = YES;
    
    self.icoImageView.centerY = headBgView.centerY - 20;
    
    self.icoImageView.centerX = headBgView.centerX;
    
    self.icoImageView.image = [UIImage imageNamed:@"face"];
    
    self.icoImageView.userInteractionEnabled = YES;
    
    self.icoImageView.backgroundColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    
    [self.icoImageView addGestureRecognizer:tap];
    
    [headBgView addSubview:self.icoImageView];
    
    [_icoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(86, 86));
        make.centerY.equalTo(bgImg.mas_centerY);
        make.centerX.equalTo(bgImg.mas_centerX);
    }];
    
    
    //    UIView *bgBottom = [[UIView alloc] initWithFrame:CGRectMake(0, headBgView.bottom - 17, ScreenWidth, 17)];
    //
    //    [headBgView addSubview:bgBottom];
    //
    //    bgBottom.backgroundColor = [UIColor colorWithWhite:0.75 alpha:1];
    //
    
    //    _bgHead = [[UIView alloc] initWithFrame:CGRectMake(0, headBgView.bottom - 17, 0, 17)];
    //
    //    [headBgView addSubview:self.bgHead];
    //
    //    _bgHead.backgroundColor = [UIColor colorWithRed:255/255.0 green:198/255.0 blue:26/255.0 alpha:1];
    
    
    
    //
    UILabel *scoreLab = [UILabel customLablWithFrame:CGRectZero andTitle:@"考试得分" andFontNumber:11];
    scoreLab.textColor = [UIColor whiteColor];
    
    [scoreLab setContentHuggingPriority:(UILayoutPriorityRequired) forAxis:(UILayoutConstraintAxisHorizontal)];
    [scoreLab setContentCompressionResistancePriority:(UILayoutPriorityRequired) forAxis:(UILayoutConstraintAxisHorizontal)];
    [headBgView addSubview:scoreLab];
    
    [scoreLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-18.5);
        make.left.offset(13);
    }];
    
    
    self.pointLabel = [UILabel customLablWithFrame:CGRectZero andTitle:@"12.23/100" andFontNumber:11];
    _pointLabel.textColor = [UIColor whiteColor];
    _pointLabel.textAlignment = NSTextAlignmentRight;
    
    [_pointLabel setContentHuggingPriority:(UILayoutPriorityRequired) forAxis:(UILayoutConstraintAxisHorizontal)];
    [_pointLabel setContentCompressionResistancePriority:(UILayoutPriorityRequired) forAxis:(UILayoutConstraintAxisHorizontal)];
    [headBgView addSubview:self.pointLabel];
    
    [_pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-18.5);
        make.right.offset(-13);
    }];
    //
    //    self.pointLabel.textColor = [UIColor whiteColor];
    //
    //    self.pointLabel.textAlignment = NSTextAlignmentCenter;
    
    UIImageView *progressBgView = [[UIImageView alloc] init];
    progressBgView.image = [UIImage imageNamed:@"bg_progress"];
    self.progressBgView = progressBgView;
    [headBgView addSubview:progressBgView];
    
    [progressBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_pointLabel.mas_centerY);
        make.left.offset(70);
        make.right.offset(-70);
    }];
    
    self.progressView = [[UIImageView alloc] init];
    _progressView.image = [UIImage imageNamed:@"line_progress"];
    [headBgView addSubview:_progressView];
    
    [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(progressBgView.mas_centerY);
        make.left.equalTo(progressBgView.mas_left).offset(3);
        make.right.lessThanOrEqualTo(progressBgView.mas_right).offset(-3);
        make.width.mas_equalTo(0).priority(MASLayoutPriorityDefaultMedium);
    }];
    
    
    
    
    
    self.nameLabel = [UILabel customLablWithFrame:CGRectMake(self.icoImageView.left, self.icoImageView.bottom + 10, self.icoImageView.width, 30) andTitle:@"姓名" andFontNumber:16];
    
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    
    [headBgView addSubview:self.nameLabel];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgImg.mas_bottom).offset(11);
        make.centerX.equalTo(bgImg.mas_centerX);
    }];
    
    _nameLabel.textColor = [UIColor whiteColor];
    
    self.textBg = [[UIView alloc] initWithFrame:CGRectMake(self.nameLabel.right-3,  self.nameLabel.bottom + 12, 0, 24)];
    
    [headBgView addSubview:self.textBg];
    
    [_textBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameLabel.mas_bottom).offset(8);
        make.width.offset(60);
        make.height.offset(20);
        make.centerX.equalTo(_nameLabel.mas_centerX);
    }];
    
    self.textBg.layer.cornerRadius = 10;
    
    self.textBg.clipsToBounds = YES;
    
    self.textBg.backgroundColor = [UIColor colorWithRed:255/255.0 green:198/255.0 blue:26/255.0 alpha:1];
    
    
    
    self.levelLabel = [UILabel customLablWithFrame:CGRectMake(self.nameLabel.right,  self.nameLabel.bottom + 10, ScreenWidth/3, 20) andTitle:@"小学" andFontNumber:15];
    
    self.levelLabel.textColor = [UIColor whiteColor];
    
    
    
    [headBgView addSubview:self.levelLabel];
    
    [self.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_textBg.mas_centerX);
        make.centerY.equalTo(_textBg.mas_centerY);
    }];
    
    
    if(@available(iOS 11.0, *)) {
        
        self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, -kStatusBarHeight, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    }else{
        
        // Fallback on earlier versions
        
        self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
        
    }
    
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.tableHeaderView = headBgView;
    self.tableV.tableFooterView = [UIView new];
    
    [self.tableV registerClass:[MineCell class] forCellReuseIdentifier:@"MineCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MineCell" forIndexPath:indexPath];
    
    cell.str = self.arr[indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%zd_mine",indexPath.row + 1]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.row == 0) {
    //
    //        VoteListController *voteList = [VoteListController new];
    //
    //        voteList.isAll = YES;
    //
    //        voteList.hidesBottomBarWhenPushed = YES;
    //
    //        [self.navigationController pushViewController:voteList animated:YES];
    //
    //    }else
    
    //@"我的学习",@"我的答题", @"我的党费
    if([self.arr[indexPath.row] isEqualToString:@"我的评议"]) {
        
        MineReviewedController *mineReviewed = [MineReviewedController new];
        //MineReviewedScoreController *mineReviewed = [MineReviewedScoreController new];
        //mineReviewed.discussionsM = self.mineModel.discussions;
        mineReviewed.discussionsM = self.mineModel.discussions;
        mineReviewed.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:mineReviewed animated:YES];
        
        
    }
    else if ([self.arr[indexPath.row] isEqualToString:@"我的学习"]) {
        
        MineListController *mineList = [MineListController new];
        
        mineList.PocketTutorPoint = self.mineModel.PocketTutorPoint;
        
        mineList.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:mineList animated:YES];
        
        
    }else if ([self.arr[indexPath.row] isEqualToString:@"我的答题"]) {
        
        MineExamController *mineExam = [MineExamController new];
        
        mineExam.exam = self.mineModel.PalmExam;
        
        mineExam.titleStr = @"我的答题";
        
        mineExam.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:mineExam animated:YES];
        
    }else if ([self.arr[indexPath.row] isEqualToString:@"我的考试"]) {
        
        MineExamController *mineExam = [MineExamController new];
        
        mineExam.exam = self.mineModel.UncorruptedExam;
        
        mineExam.titleStr = @"我的考试";
        
        mineExam.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:mineExam animated:YES];
        
    }else if ([self.arr[indexPath.row] isEqualToString:@"党费计算"]) {
        
        //        MyPartyMembershipDuesVC *vc = [[MyPartyMembershipDuesVC alloc] init];
        //        vc.hidesBottomBarWhenPushed = YES;
        //        vc.title = @"党费计算";
        //
        //        [self.navigationController pushViewController:vc animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"敬请期待" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [alert show];
        [self performSelector:@selector(dimissAlert:)withObject:alert afterDelay:2.0];
        
        
    }
    
}
- (void) dimissAlert:(UIAlertView *)alert {
    if(alert){
        [alert dismissWithClickedButtonIndex:[alert cancelButtonIndex] animated:YES];
    }
}
- (void)setClick {
    
    MineSetController *mineSet = [MineSetController new];
    
    mineSet.hidesBottomBarWhenPushed = YES;
    
    mineSet.aboutMe = self.mineModel.about;
    
    [self.navigationController pushViewController:mineSet animated:YES];
}

- (void)tapClick {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:(UIAlertControllerStyleActionSheet)];
    UIAlertAction *phontoListAction = [UIAlertAction actionWithTitle:@"从相册选取" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    UIAlertAction *camereAction = [UIAlertAction actionWithTitle:@"从相机拍照选择" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    
    [alertController addAction:phontoListAction];
    [alertController addAction:camereAction];
    [alertController addAction:cancleAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    self.photoImageView = image;
    
    [MBProgressHUD showProgressMessage:@"正在上传头像" RemainTime:0 ToView:[UIApplication sharedApplication].keyWindow userInteractionEnabled:YES];
    
    [DataRequest PostWithURL:UpLoadIco parameter:nil andImage:self.photoImageView:^(id responseObject) {
        
        DLog(@"%@",responseObject);
        
        if ([responseObject[@"result"] isEqualToString:@"uploadSuccess"]) {
            
            [MBProgressHUD showMessage:@"上传成功" RemainTime:1 ToView:[UIApplication sharedApplication].keyWindow userInteractionEnabled:YES];
            
            
            [picker dismissViewControllerAnimated:YES completion:nil];
            
        }else {
            
            
        }
        
        
    } failure:^(NSError *error) {
        DLog(@"%@",error);
    }];
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)navigationController:(UINavigationController*)navigationController willShowViewController:(UIViewController*)viewController animated:(BOOL)animated
{
    if(viewController ==self){
        [_navController setNavigationBarHidden:YES animated:YES];
    }else{
        //不在本页时，显示真正的nav bar
        [_navController setNavigationBarHidden:NO animated:NO];
        //当不显示本页时，要么就push到下一页，要么就被pop了，那么就将delegate设置为nil，防止出现BAD ACCESS
        //之前将这段代码放在viewDidDisappear和dealloc中，这两种情况可能已经被pop了，self.navigationController为nil，这里采用手动持有navigationController的引用来解决
        if(_navController.delegate==self){
            //如果delegate是自己才设置为nil，因为viewWillAppear调用的比此方法较早，其他controller如果设置了delegate就可能会被误伤
            _navController.delegate=nil;
        }
    }
}

@end


