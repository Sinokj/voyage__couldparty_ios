//
//  MineListController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/12/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineListController.h"
#import "ListCell.h"
#import "WebViewController.h"
#import <Masonry.h>

@interface MineListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) NSIndexPath *indexp;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) NSMutableArray<NewList *> *sourceArr;

@end

@implementation MineListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.sourceArr = [NSMutableArray array];
    
    self.title = @"我的学习";
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
    
    @weakify(self);
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithPage:1];
        self.page = 1;
    }];
    self.tableV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        self.page +=1;
        [self requestDataWithPage:self.page];
    }];
    self.page = 1;
    
    
    [self requestDataWithPage:1];
    
    
}

#warning 接口改了
- (void)requestDataWithPage:(NSInteger)pageNo {
    
    [DataRequest GetWithURL:[NSString stringWithFormat:@"article/getArtileLog.do?nPageNo=%ld&nPageSize=10&nCommitteeId=%@",(long)pageNo,[STUtils objectForKey:KDangJianType]?[STUtils objectForKey:KDangJianType]:@2] :^(id reponserObject) {
        
        ListModel *listM = [ListModel mj_objectWithKeyValues:reponserObject];
        
        if (listM.objects.count == 0 || listM.objects == nil) {

        [MBProgressHUD showMessage:@"暂无数据" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        }
        if (pageNo > 1) {
            [self.sourceArr addObjectsFromArray:listM.objects];
        }else {
            [self.sourceArr removeAllObjects];
            [self.sourceArr addObjectsFromArray:listM.objects];
        }
        if (reponserObject[@"objects"] == nil || [reponserObject[@"objects"] count] == 0) {
            [MBProgressHUD showMessage:@"暂无数据" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        }
        
        [self.tableV reloadData];
        [self.tableV.mj_header endRefreshing];
        [self.tableV.mj_footer endRefreshing];
    }];
} 

- (void)prepareLayout {
    
    UIImageView *imgV = [[UIImageView alloc] init];
    [self.view addSubview:imgV];
    
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.offset(0);
        make.height.mas_equalTo(100);
    }];
    
    imgV.image = [UIImage imageNamed:@"bg_anwser1"];
    
    imgV.contentMode = UIViewContentModeRedraw;
    
    
    UILabel *scoreL = [UILabel customLablWithFrame:CGRectZero andTitle:@"当前学习所得分数" andFontNumber:16];
        scoreL.textColor = [UIColor whiteColor];
    [imgV addSubview:scoreL];
    [scoreL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(20);
        make.centerX.offset(0);
    }];
    
    scoreL.textAlignment = NSTextAlignmentCenter;
    
    UILabel *scoreLab = [[UILabel alloc] init];
    
    scoreLab.font = [UIFont systemFontOfSize:25];
    
    scoreLab.text = [NSString stringWithFormat:@"%.2f",[self.PocketTutorPoint floatValue]];
    scoreLab.textAlignment = NSTextAlignmentCenter;
    scoreLab.textColor = [UIColor whiteColor];
    [imgV addSubview:scoreLab];
    
    [scoreLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scoreL.mas_bottom).offset(14);
        make.centerX.offset(0);
    }];
    
    
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    
    
    self.tableV.tableFooterView = [UIView new];
//    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableV registerNib:[UINib nibWithNibName:@"ListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [_tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.equalTo(self.view);
        make.top.equalTo(imgV.mas_bottom);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.isLearn = YES;
    cell.model = self.sourceArr[indexPath.row];
    cell.seeLabel.hidden = YES;
    cell.alertL.hidden = YES;
    cell.timeLeftMarginConstraint.priority = 750;
    cell.timeLeftToSuperViewConstraint.priority = 999;

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
    
    WebViewController *webVC = [WebViewController new];
    
    webVC.type = @"1";
    webVC.str = self.sourceArr[indexPath.row].url;
    
    NewList *neList = self.sourceArr[indexPath.row];
    
    
    webVC.canShare = neList.nShared;
    
    webVC.tit = @"  ";
    
    webVC.shareImage = neList.vcPath;
    webVC.shareTitle = neList.vcTitle;
    webVC.shareDescribe = neList.vcDescribe;
    
    webVC.articleId = neList.nId;
    webVC.vcType = neList.vcType;
    
    self.indexp = indexPath;
    
    [self.navigationController pushViewController:webVC animated:YES];
    
}

//- (void)getNewState:(NSInteger)state {
//    
//    self.sourceArr[self.indexp.row].nStatus = state;
//    
//    [self.tableV reloadRowsAtIndexPaths:@[self.indexp] withRowAnimation:(UITableViewRowAnimationNone)];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110;
}


@end
