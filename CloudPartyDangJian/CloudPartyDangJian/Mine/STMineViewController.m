//
//  STMineViewController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "STMineViewController.h"
#import "STMineCell.h"
#import "MineModel.h"
#import "MineSetController.h"
#import "MineListController.h"
#import "MineExamController.h"
#import "STDangFeiViewController.h"
#import "STTuShuBiViewController.h"
#import "MyMessageVC.h"
#import "DESaoMIaoController.h"
#import "PartyArticleListModel.h"
@implementation STMineItem
+ (STMineItem *)mineItemWithImageName:(NSString *)imageName categoryName:(NSString *)categoryName {
    STMineItem *item = [STMineItem new];
    item.imageIcon = [UIImage imageNamed:imageName];
    item.categoryName = categoryName;
    return item;
}
@end


// cellID
static NSString *const STMineCellID = @"STMineCellID";
@interface STMineViewController () <UITableViewDelegate,UITableViewDataSource>
#pragma mark - properties
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray<STMineItem *> *items; //存储所有的分类items
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView; //头像
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel; //用户姓名
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel; //部分名称
@property (weak, nonatomic) IBOutlet UILabel *levelLabel; //学习等级
@property (weak, nonatomic) IBOutlet UILabel *levelPointLabel; //学习得分
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *groupLeftMargin;
@property (nonatomic, strong) MineModel *mineModel; //用户信息模型
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint; //tableview高度约束
@end

@implementation STMineViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 请求用户信息
    [self requestUserInfo];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // tableView初始化
    [self setupTableView];
    // 头像圆角边框
    [self setupAvatarStyle];
    // 初始化导航栏
    [self setupNavStyle];
    // 初始化一些点击事件
    [self setupEventAction];
}

#pragma mark - fetchUserInfo
// 请求用户信息
- (void)requestUserInfo {
    [MBProgressHUD hideHUD];
    [DataRequest CatlikeGetWithURL:MineInfo :^(id reponserObject) {
        // 字典转模型
        self.mineModel = [MineModel mj_objectWithKeyValues:reponserObject];
        // 更新用户信息
        [self updateUserInfo];
    }];
}

// 更新用户信息
- (void)updateUserInfo {
    // 头像
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.mineModel.baseInfo.vcHeadImgUrl] placeholderImage:[UIImage imageNamed:@"face"]];
    // 名字
    self.userNameLabel.text = self.mineModel.baseInfo.vcName;
    // 所在组
    self.groupNameLabel.text = self.mineModel.baseInfo.vcDeptName;
    // 学习等级
    self.levelLabel.text = self.mineModel.level.vcLevel;
    // 学习分数
    self.levelPointLabel.text = [NSString stringWithFormat:@"%.2f",self.mineModel.nLevelPoint];
    
    // 组名过长
    if (self.groupNameLabel.text.length > 11) {
        self.groupLeftMargin.constant = 25;
    }else {
        self.groupLeftMargin.constant = 0;
    }
}

#pragma mark - Lazy
- (NSArray<STMineItem *> *)items {
    if (_items == nil) {
        _items = @[[STMineItem mineItemWithImageName:@"icon_my_study" categoryName:@"我的学习"],
                   [STMineItem mineItemWithImageName:@"icon_my_answer" categoryName:@"我的答题"],
                   [STMineItem mineItemWithImageName:@"icon_my_money" categoryName:@"我的党费"],
                   [STMineItem mineItemWithImageName:@"my_msg" categoryName:@"我的消息"],
                   [STMineItem mineItemWithImageName:@"my_saoma" categoryName:@"扫码签到"]
//                   [STMineItem mineItemWithImageName:@"icon_my_money" categoryName:@"我的图书币"]
                   ];
    }
    return _items;
}

#pragma mark - 初始化
// tableView初始化
- (void)setupTableView {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 65;
    self.tableViewHeightConstraint.constant = 65 * self.items.count;
    //注册xib
    [self.tableView registerNib:[UINib nibWithNibName:@"STMineCell" bundle:nil] forCellReuseIdentifier:STMineCellID];
}

// 用户头像
- (void)setupAvatarStyle {
    self.avatarImageView.layer.borderWidth = 1;
    self.avatarImageView.layer.borderColor = AppTintColor.CGColor;
    self.avatarImageView.layer.cornerRadius = 71/2;
    self.avatarImageView.clipsToBounds = YES;
}

// 导航栏style
- (void)setupNavStyle {
    self.title = @"个人中心";
    self.tabBarItem.title = @"我的";
    // rightIem
    @weakify(self);
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"set2" block:^(id sender) {
        @strongify(self);
        // 去往设置界面
        MineSetController *mineSet = [MineSetController new];
        mineSet.aboutMe = self.mineModel.about;
        [self.navigationController pushViewController:mineSet animated:YES];
    }];
}

#pragma mark - Event
- (void)setupEventAction {
    self.avatarImageView.userInteractionEnabled = YES;
    @weakify(self);
    [self.avatarImageView bk_whenTapped:^{
        @strongify(self);
        // 获取头像
        [STUtils imagePickerWithVc:self maxImagesCount:1 finishPickingBlock:^(NSArray<UIImage *> *photos) {
            DDLog(@"%@",photos);
            @strongify(self);
            // 上传头像
            if(photos && photos.count > 0) {
                [self uploadUserAvatar:photos[0]];
            }
        }];
    }];
}

#pragma mark - Extra Private
// 上传用户头像到服务器
- (void)uploadUserAvatar:(UIImage *)avatarImage {
    
    // 首先直接显示新头像
    dispatch_async(dispatch_get_main_queue(), ^{
        self.avatarImageView.image = avatarImage;
    });
    
    // 上传
    [DataRequest PostWithURL:UpLoadIco parameter:nil andImage:[avatarImage imageByResizeToSize:CGSizeMake(100, 100)]:^(id responseObject) {
        DLog(@"%@",responseObject);
        if ([responseObject[@"result"] isEqualToString:@"uploadSuccess"]) {
            self.avatarImageView.image = avatarImage;
            [MBProgressHUD showMessage:@"上传头像成功" RemainTime:1 ToView:[UIApplication sharedApplication].keyWindow userInteractionEnabled:YES];
        }else {
        }
        
    } failure:^(NSError *error) {
        DLog(@"%@",error);
    }];
}

// 跳转到我的学习
- (void)navigateToMyStudy {
    MineListController *mineList = [MineListController new];
    mineList.PocketTutorPoint = @(self.mineModel.baseInfo.nPocketTutorPoint).description;
    [self.navigationController pushViewController:mineList animated:YES];
}

// 跳转到我的答题
- (void)navigateToMyExam {
    MineExamController *mineExam = [MineExamController new];
    mineExam.exam = self.mineModel.PalmExam;
    mineExam.titleStr = @"我的答题";
    [self.navigationController pushViewController:mineExam animated:YES];
}

// 跳转到我的党费
- (void)navigateToMyRepete {
    STDangFeiViewController *party = [STDangFeiViewController new];
    [self.navigationController pushViewController:party animated:YES];
}

// 我的消息
- (void)navigateToMyMsg{
    MyMessageVC *party = [MyMessageVC new];
    [self.navigationController pushViewController:party animated:YES];
}

// 扫码签到
- (void)navigateToSign{
    DESaoMIaoController *saomaVC = [DESaoMIaoController new];
    saomaVC.isVideoZoom = YES;
    saomaVC.resultBlock = ^(NSString *scanResult) {
        //字符串解密
        NSData *dataFromBase64String = [[NSData alloc]
                                        initWithBase64EncodedString:scanResult options:0];
        NSString *base64Decoded = [[NSString alloc]
                                   initWithData:dataFromBase64String encoding:NSUTF8StringEncoding];
        
        
        PartyArticle *model = [PartyArticle  mj_objectWithKeyValues:base64Decoded];
        
        
        NSString *url = [NSString stringWithFormat:@"%@nId=%ld",SanHuiYiKeSignIn, model.nId];
        [DataRequest GetWithURL:url :^(id reponserObject) {
            [SMGlobalMethod showMiddleMessage:reponserObject[@"vcResult"]];
        }];
    };
    [self.navigationController pushViewController:saomaVC animated:YES];
}

// 跳转到我图书币
- (void)navigateToMyDfb {
    STTuShuBiViewController *party = [STTuShuBiViewController new];
    [self.navigationController pushViewController:party animated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    STMineCell *cell = [tableView dequeueReusableCellWithIdentifier:STMineCellID forIndexPath:indexPath];
    [cell setMineItem:_items[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        // 我的学习
        [self navigateToMyStudy];
    } else if (indexPath.row == 1) {
        // 我的答题
        [self navigateToMyExam];
    } else if (indexPath.row == 2){
        // 我的竞赛
        [self navigateToMyRepete];
    }else if(indexPath.row == 3){
        // 我的消息
        [self navigateToMyMsg];
    }else{
        //扫码签到
        [self navigateToSign];
    }
}
@end
