//
//  STTuShuBiViewController.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/9.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "STTuShuBiViewController.h"
#import "SMTushbiCell.h"
#import "STTushbiModel.h"

 static NSString * SMTushbiCellID = @"SMTushbiCell";

@interface STTuShuBiViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *modelArray;
@end

@implementation STTuShuBiViewController{
    UILabel *scoreLab;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的图书币";
    [self prepareLayout];
    
    [self fetchData];
    [self fetchRecordData];
}

- (void)prepareLayout {
    
    UIImageView *imgV = [[UIImageView alloc] init];
    [self.view addSubview:imgV];
    
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.offset(0);
        make.height.mas_equalTo(100);
    }];
    
    imgV.image = [UIImage imageNamed:@"bg_anwser1"];
    
    imgV.contentMode = UIViewContentModeRedraw;
    
    
    UILabel *scoreL = [UILabel customLablWithFrame:CGRectZero andTitle:@"当前图书币" andFontNumber:16];
    scoreL.textColor = [UIColor whiteColor];
    [imgV addSubview:scoreL];
    [scoreL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(20);
        make.centerX.offset(0);
    }];
    
    scoreL.textAlignment = NSTextAlignmentCenter;
    
    scoreLab = [[UILabel alloc] init];
    
    scoreLab.font = [UIFont systemFontOfSize:25];
    
    scoreLab.textAlignment = NSTextAlignmentCenter;
    scoreLab.textColor = [UIColor whiteColor];
    [imgV addSubview:scoreLab];
    
    [scoreLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scoreL.mas_bottom).offset(14);
        make.centerX.offset(0);
    }];
    
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    self.tableView.delegate = self;
    
    self.tableView.dataSource = self;
    
    
    
    self.tableView.tableFooterView = [UIView new];
    //    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SMTushbiCell" bundle:nil] forCellReuseIdentifier:@"SMTushbiCell"];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.equalTo(self.view);
        make.top.equalTo(imgV.mas_bottom);
    }];
}

//余额
- (void)fetchData {
    [DataRequest GetWithURL:GetMyTokenMoney :^(id reponserObject) {
        if(reponserObject[@"objects"]) {
            scoreLab.text = [NSString stringWithFormat:@"%@",reponserObject[@"objects"]];
        }
    }];
}

//消费记录
- (void)fetchRecordData {
    [DataRequest GetWithURL:GetMyTokenPayLog :^(id reponserObject) {
        if(reponserObject[@"objects"]) {
            NSArray *temp = [STTushbiModel mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]];
            [self.modelArray addObjectsFromArray:temp];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - DELEGATE DATA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.modelArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SMTushbiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMTushbiCell"];
    STTushbiModel *model = self.modelArray[indexPath.row];
//    cell.timeLabel.text = dangFei.dtExe;
//    NSDate *date = [NSDate dateWithString:dangFei.dtExe format:@"YYYY-MM-dd HH:mm:ss"];
//
//    cell.nameLabel.text = [NSString stringWithFormat:@"%@年%@月份党费",@(date.year),@(date.month)];
//    cell.moneyLabel.text = [NSString stringWithFormat:@"%.2f元",dangFei.mRealFee];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}


- (NSMutableArray *)modelArray{
    if (_modelArray == nil) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
