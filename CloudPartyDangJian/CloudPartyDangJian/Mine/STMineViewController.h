//
//  STMineViewController.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

/**
 cell的Item
 */
@interface STMineItem: NSObject
// 图片
@property (nonatomic, retain) UIImage *imageIcon;
// 分类名字
@property (nonatomic, copy) NSString *categoryName;
+ (STMineItem *)mineItemWithImageName:(NSString *)imageName
                         categoryName:(NSString *)categoryName;
@end

/**
 新的我的页面用来替换 MineController 
 */
@interface STMineViewController : ViewController

@end
