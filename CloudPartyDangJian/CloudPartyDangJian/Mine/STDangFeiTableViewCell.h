//
//  STDangFeiTableViewCell.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/31.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STDangFeiTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end
