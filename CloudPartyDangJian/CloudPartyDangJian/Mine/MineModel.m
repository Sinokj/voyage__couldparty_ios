//
//  MineModel.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineModel.h"

@implementation MineModel


+ (NSDictionary *)objectClassInArray{
    return @{@"discussions" : [Discussions class],
             @"exam" : [MineExam class],
             @"PalmExam" : [MineExam class],
             @"UncorruptedExam" : [MineExam class],
             @"level" : [Level class]
             };
}
@end

@implementation Baseinfo

@end


@implementation Discussions

+ (NSDictionary *)objectClassInArray{
    return @{@"content" : [Content class]};
}

@end


@implementation Content

@end

@implementation Level

@end

@implementation MineExam

@end


