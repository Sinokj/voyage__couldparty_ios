//
//  SMTushbiCell.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/9.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTushbiModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMTushbiCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tiemLable;
@property (nonatomic, strong)STTushbiModel *model;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@end

NS_ASSUME_NONNULL_END
