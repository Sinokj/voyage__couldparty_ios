
//
//  MineCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineCell.h"
#import "DyfTool.h"

@interface MineCell ()

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIImageView *imageV;

@end
@implementation MineCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.imageV = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 30, 30)];
    
    [self.contentView addSubview:self.imageV];
    
    
    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(self.imageV.right + 10, 0, ScreenWidth - 60, 50) andTitle:@"" andFontNumber:15];
    
    [self.contentView addSubview:self.titleLabel];;
}

- (void)setStr:(NSString *)str {
    if (_str != str) {
        _str = nil;
        _str = str;
        
        self.titleLabel.text = str;
    }
}

-(void)setImage:(UIImage *)image {
    if (_image != image) {
        _image = nil;
        _image = image;
        
        self.imageV.image = image;
    }
}

@end
