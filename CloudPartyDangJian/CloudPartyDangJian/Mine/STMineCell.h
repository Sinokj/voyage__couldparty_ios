//
//  STMineCell.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STMineViewController.h"

/**
 个人中心的cell
 */
@interface STMineCell : UITableViewCell

/**
 设置cell的Item

 @param mineItem STMineItem类型
 */
- (void)setMineItem:(STMineItem*)mineItem;
@end
