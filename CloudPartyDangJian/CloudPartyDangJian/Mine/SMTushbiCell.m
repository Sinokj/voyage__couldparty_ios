//
//  SMTushbiCell.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/9.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "SMTushbiCell.h"

@implementation SMTushbiCell

- (void)setModel:(STTushbiModel *)model{
    _model = model;
    self.NameLabel.text = [NSString stringWithFormat:@"%@",model.vcType] ;
    self.tiemLable.text = model.dtReg;
    self.priceLabel.text = [NSString stringWithFormat:@"%@",model.nTokenFee];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
