//
//  MineSetController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineSetController.h"
#import "WebViewController.h"
#import "ResetPWController.h"
#import <Masonry.h>
#import "NSObject+rootVcExtension.h"
@interface MineSetController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) NSArray *arr;

@end

@implementation MineSetController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"系统设置";
    
    self.arr = @[@[@"  密码安全"],@[@"  声音"],@[@"  震动"],@[@"  当前版本"]];
    
    [self prepareLayout];
}

- (void)prepareLayout {
    
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 205) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.scrollEnabled = NO;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [self.tableV registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];

    [self.tableV registerClass:[UITableViewCell class] forCellReuseIdentifier:@"firstcell"];
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [btn setBackgroundImage:[UIImage imageNamed:@"exit_login"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"exit_login"] forState:UIControlStateHighlighted];
    
    [btn addTarget:self action:@selector(loginOutBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:btn];
    @weakify(self);
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weak_self.view.mas_centerX);
        make.width.mas_equalTo(MYDIMESCALEW(275));
        make.height.mas_equalTo(MYDIMESCALEH(40));
        make.top.equalTo(weak_self.tableV.mas_bottom).offset(MYDIMESCALEH(100));
    }];
    
    UIView *lineV = [UIView new];
    lineV.backgroundColor = UIColorHex(f5f5f5);
    [self.view addSubview:lineV];
    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.mas_equalTo(weak_self.tableV.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 3) {
        return 2;
    }else
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"firstcell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = UIColorHex(f5f5f5);
        return cell;

    }else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell removeAllSubviews];
        if (indexPath.section == 3) {
            
            UILabel *label = [UILabel customLablWithFrame:CGRectMake(12, 7.5, ScreenWidth/2, 30) andTitle:self.arr[indexPath.section][indexPath.row - 1] andFontNumber:16];
            
            NSDictionary* infoDic = [[NSBundle mainBundle] infoDictionary];
            NSString* currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];//当前版本号
            
            UILabel *right = [UILabel customLablWithFrame:CGRectMake(ScreenWidth/2, 7.5, ScreenWidth/2 - 20, 30) andTitle:[NSString stringWithFormat:@"V%@",currentVersion] andFontNumber:16];
            
            right.textAlignment = NSTextAlignmentRight;
            
            [cell addSubview:right];
            
            [cell addSubview:label];
            
        }else if (indexPath.section == 0) {
            
            UILabel *label = [UILabel customLablWithFrame:CGRectMake(12, 7.5, ScreenWidth/2, 30) andTitle:self.arr[indexPath.section][indexPath.row - 1] andFontNumber:16];
            
            [cell addSubview:label];
            
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth - 25 - 12, 12.5, 25, 25)];
            
            imageV.image = [UIImage imageNamed:@"icon_arrow"];
            
            [cell addSubview:imageV];
            
        }else {
            
            UILabel *label = [UILabel customLablWithFrame:CGRectMake(12, 7.5, ScreenWidth/2, 30) andTitle:self.arr[indexPath.section][indexPath.row -1] andFontNumber:16];
            
            UISwitch *swich = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenWidth - 60, 7.5, 40, 30)];
            id voiceOrShake = nil;
            if ([label.text isEqualToString:@"  声音"]) {
                voiceOrShake = [STUtils objectForKey:KSetVoice];
                if (voiceOrShake == nil || [voiceOrShake boolValue] == YES) {
                    swich.on = YES;
                }else {
                    swich.on = NO;
                }
            }else {
                voiceOrShake = [STUtils objectForKey:KSetShake];
                if (voiceOrShake == nil || [voiceOrShake boolValue] == YES) {
                    swich.on = YES;
                }else {
                    swich.on = NO;
                }

            }
            @weakify(self);
            [swich removeAllBlocksForControlEvents:UIControlEventValueChanged];
            [swich addBlockForControlEvents:UIControlEventValueChanged block:^(UISwitch  * sender) {
                if ([weak_self.arr[indexPath.section][indexPath.row -1] isEqualToString:@"  声音"]) {
                    [STUtils setObject:@(sender.on) forKey:KSetVoice];
                }else {
                    [STUtils setObject:@(sender.on) forKey:KSetShake];
                }
            }];
            
            
            swich.tag = indexPath.section + 1000;
            
            [cell addSubview:swich];
            
            [cell addSubview:label];
            
            
        }
        return cell;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 || indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 12;
        }else{
            return 45;
        }
    }else {
        if (indexPath.row == 0) {
            return 1;
        }else{
            return 45;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 3) {
        
        return;
        
//        WebViewController *web = [WebViewController new];
//        
//        web.type = @"1";
//        
//        web.str = self.aboutMe;
//        
//        web.tit = @"关于我们";
//        
//        [self.navigationController pushViewController:web animated:YES];
    }else if (indexPath.section == 0) {
        
        ResetPWController *reset = [ResetPWController new];
        
        [self.navigationController pushViewController:reset animated:YES];
    }
}


- (void)loginOutBtnClick {
 
    [DataRequest PostWithURL:LoginOut parameter:nil :^(id reponserObject) {
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"result"]] isEqualToString:@"1"]) {
            
            
            //登录状态
            [STUtils setObject:@0 forKey:KIsLogin];
            
            //清楚cookie
            [STUtils clearCookie];
            
            [MBProgressHUD showMessage:@"退出成功" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                
                [self.homeVc updateInfoAfterLoginIn:@"2"];
                
                [STUtils setObject:@"2" forKey:KDangJianType];
                self.tabBarVc.selectedIndex = 0;
                [self.bullVc reloadData];
                
                [self.webVc.webV reload];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.tabBarVc.tabBar.hidden = NO;
                    [self.navigationController popViewControllerAnimated:NO];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginUpdateData" object:nil userInfo:nil];
                });
                
                
            });
            
        }else {
//            [MBHelper addHUDInView:self.view text:reponserObject[@"vcRes"]];
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:2 ToView:self.view userInteractionEnabled:YES];
            
        }

        
    }];
}

@end
