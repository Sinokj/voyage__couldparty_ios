//
//  ResetPWController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/8/1.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ResetPWController.h"

@interface ResetPWController ()

//@property (nonatomic, strong) UITextField *oldPw;
@property (weak, nonatomic) IBOutlet UITextField *oldPw;

//@property (nonatomic, strong) UITextField *setPw;
@property (weak, nonatomic) IBOutlet UITextField *setPw;

//@property (nonatomic, strong) UITextField *aginPw;
@property (weak, nonatomic) IBOutlet UITextField *aginPw;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTopConstraint;

@end

@implementation ResetPWController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"密码安全";
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    [self.oldPw becomeFirstResponder];
//    [self prepareLayout];
    [STUtils setupView:self.resetBtn cornerRadius:MYDIMESCALEW(20) bgColor:AppTintColor borderW:0 borderColor:nil];
    self.btnTopConstraint.constant = MYDIMESCALEH(300);
}

- (void)prepareLayout {
    
    self.view.backgroundColor = [UIColor colorWithRed:0.941 green:0.9365 blue:0.9456 alpha:1.0];
    
    self.oldPw = [self customWithFrame:CGRectMake(20, 80, ScreenWidth - 40, 40) andLeftTitle:@"  原密码" andPlaceH:@"请输入原登录密码"];
    [self.oldPw becomeFirstResponder];
    
    [self.view addSubview:self.oldPw];
    
    self.setPw = [self customWithFrame:CGRectMake(20, self.oldPw.bottom + 10, ScreenWidth - 40, 40) andLeftTitle:@"  新密码" andPlaceH:@"请输入新密码"];
    
    [self.view addSubview:self.setPw];
    
    self.aginPw = [self customWithFrame:CGRectMake(20, self.setPw.bottom + 10, ScreenWidth - 40, 40) andLeftTitle:@"  确认密码" andPlaceH:@"请再次输入新密码"];
    
    [self.view addSubview:self.aginPw];
    
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeSystem)];
    
    btn.frame = CGRectMake(20, self.aginPw.bottom + 20, ScreenWidth - 40, 40);
    
    btn.backgroundColor = AppTintColor;
    
    btn.layer.cornerRadius = 3;
    
    [btn setTintColor:[UIColor whiteColor]];
    
    [btn setTitle:@"确  认  修  改" forState:(UIControlStateNormal)];
    
    [btn addTarget:self action:@selector(resetBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.view addSubview:btn];
}
- (IBAction)resetBtnClick2:(UIButton *)sender {
    [self resetBtnClick];
}

-  (void)resetBtnClick {
    if (![self.setPw.text isEqualToString:self.aginPw.text]) {
        
        [MBProgressHUD showMessage:@"两次密码不一致" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        
        return;
        
    }else if(self.setPw.text == nil || [self.setPw.text isEqualToString:@""] || self.oldPw.text == nil || [self.oldPw.text isEqualToString:@""] || self.aginPw.text == nil || [self.aginPw.text isEqualToString:@""]) {
        
        [MBProgressHUD showMessage:@"请填写全部信息" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        return;
    }
    
    [DataRequest PostWithURL:ResetPassWord parameter:@{@"oldPassword": self.oldPw.text, @"newPassword" : self.aginPw.text} :^(id reponserObject) {
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else {
            
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
            
        }

    }];
}

- (UITextField *)customWithFrame:(CGRect)frame andLeftTitle:(NSString *)leftT andPlaceH:(NSString *)placeho {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 100, frame.size.height - 20)];
    label.text = leftT;
    label.textColor = [UIColor colorWithRed:0.4353 green:0.4353 blue:0.4353 alpha:1.0];
    label.font = [UIFont systemFontOfSize:15];
    UITextField *textF = [[UITextField alloc] initWithFrame:frame];
    textF.leftViewMode = UITextFieldViewModeAlways;
    textF.layer.borderWidth = 0;
    textF.leftView = label;
    textF.placeholder = placeho;
    textF.clearButtonMode = UITextFieldViewModeWhileEditing;
    textF.font = [UIFont systemFontOfSize:13];
//    [self addLineWithUI:textF andFrame:CGRectMake(0, frame.size.height - 1, frame.size.width, 1) andColor:[UIColor colorWithRed:0.7019 green:0.7019 blue:0.7019 alpha:1.0]];
    textF.backgroundColor = [UIColor whiteColor];
    textF.delegate = self;
    textF.borderStyle = UITextBorderStyleRoundedRect;
    textF.secureTextEntry = YES;
    return textF;
}


- (void)addLineWithUI:(UIView *)UIv andFrame:(CGRect)frame andColor:(UIColor *)color {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = color;
    view.frame = frame;
    [UIv addSubview:view];
}


@end
