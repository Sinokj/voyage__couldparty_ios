//
//  MinePingYiCell.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/13.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "thoughtModel.h"
@interface MinePingYiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *duiXiangLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property(nonatomic,strong)thought *thought;
@end
