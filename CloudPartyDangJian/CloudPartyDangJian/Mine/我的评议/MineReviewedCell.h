//
//  MineReviewedCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/9/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineModel.h"

@interface MineReviewedCell : UITableViewCell

@property (nonatomic, strong) Discussions *discuss;

@end
