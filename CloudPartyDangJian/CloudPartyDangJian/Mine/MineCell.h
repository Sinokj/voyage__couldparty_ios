//
//  MineCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineCell : UITableViewCell

@property (nonatomic, strong) NSString *str;

@property (nonatomic, strong) UIImage *image;

@end
