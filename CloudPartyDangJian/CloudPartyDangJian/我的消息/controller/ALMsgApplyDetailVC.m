//
//  ALMsgApplyDetailVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "ALMsgApplyDetailVC.h"
#import "ALMyMsgModel.h"

@interface ALMsgApplyDetailVC ()

@property (nonatomic, strong)UILabel *firstLabel;
@property (nonatomic, strong)UILabel *secondLabel;
@property (nonatomic, strong)UILabel *thirdLabel;
@property (nonatomic, strong)UILabel *forthLabel;
@property (nonatomic, strong)UILabel *endLabel;

@property (nonatomic, strong)UIButton *btmFirBtn;
@property (nonatomic, strong)UIButton *btmSecBtn;

/** 印记 */
@property (nonatomic, strong)UIImageView *agreeImageView;

@property (nonatomic, strong)ALMyMsgModel *model;
@end

@implementation ALMsgApplyDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    //title
    self.title = @"申请详情";
    UIBarButtonItem *leftItem = [UIBarButtonItem barButtonItemWithImageName:@"back" block:^(id sender) {
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    //绘制界面
    [self drawView];
    //加载详情
    if ([self.type isEqualToString:@"请假通知"]) {
        [self requestForDetail];
    }else if ([self.type isEqualToString:@"审核通知"]) {
        [self requestForApply];
    }
}

//请假
- (void)requestForDetail{
    NSString *url = [NSString stringWithFormat:@"%@nId=%ld",AuditLeave, self.nId];
    [DataRequest GetWithURL:url :^(id reponserObject) {
        self.model = [ALMyMsgModel mj_objectWithKeyValues:reponserObject];
        
        self.firstLabel.text = self.model.vcName;
        self.secondLabel.text = self.model.meetingName;
        self.thirdLabel.text = self.model.vcTel;
        self.forthLabel.text = self.model.leaveTime;
        self.endLabel.text = self.model.vcReason;
        
        if (self.model.nStatus == 0) {//0未处理 1 审核通过 2审核未通过
            self.btmFirBtn.hidden = NO;
            self.btmSecBtn.hidden = NO;
            self.agreeImageView.hidden = YES;
        }else if(self.model.nStatus == 1){
            self.btmFirBtn.hidden = YES;
            self.btmSecBtn.hidden = YES;
            self.agreeImageView.hidden = NO;
            self.agreeImageView.image = [UIImage imageNamed:@"apply_agree"];
        }else if(self.model.nStatus == 2){
            self.btmFirBtn.hidden = YES;
            self.btmSecBtn.hidden = YES;
            self.agreeImageView.hidden = NO;
            self.agreeImageView.image = [UIImage imageNamed:@"apply_disagree"];
        }
    }];
}

//审核
- (void)requestForApply{
    NSString *url = [NSString stringWithFormat:@"%@nID=%ld",HandleAudit, self.nId];
    [DataRequest GetWithURL:url :^(id reponserObject) {
        self.model = [ALMyMsgModel mj_objectWithKeyValues:reponserObject];
        
        self.firstLabel.text = self.model.vcName;
        self.secondLabel.text = self.model.vcSex;
        self.thirdLabel.text = self.model.vcTel;
        self.forthLabel.text = self.model.dtApplication;
        self.endLabel.text = self.model.vcStepName;
        
        if (self.model.nStatus == 0) {//0未处理 1 审核通过 2审核未通过
            self.btmFirBtn.hidden = NO;
            self.btmSecBtn.hidden = NO;
        }else if(self.model.nStatus == 1){
            self.btmFirBtn.hidden = YES;
            self.btmSecBtn.hidden = YES;
        }else if(self.model.nStatus == 2){
            self.btmFirBtn.hidden = YES;
            self.btmSecBtn.hidden = YES;
        }
    }];
}

//请假是否同意
- (void)requestForQingJiaAgree:(NSInteger)isAggree{//1 同意  0 拒绝
    
    NSString *url = [NSString stringWithFormat:@"%@nId=%ld&&vcResult=%ld",auditLeaveResult, self.nId,isAggree];
    [DataRequest GetWithURL:url :^(id reponserObject) {
        [SMGlobalMethod showMiddleMessage:reponserObject[@"vcResult"]];
        //推出
        if (self.popBlock) {
            self.popBlock();
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

//审核是否同意
- (void)requestForApplyAgree:(NSInteger)isAggree{
    NSString *url = [NSString stringWithFormat:@"%@nId=%ld&&vcResult=%ld&vcStepNo=%@&vcAuditNo=%@",resultAudit, self.nId,isAggree,self.model.vcStepNo,self.model.vcAuditNo];
    [DataRequest GetWithURL:url :^(id reponserObject) {
        [SMGlobalMethod showMiddleMessage:reponserObject[@"vcResult"]];
        //推出
        if (self.popBlock) {
            self.popBlock();
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)drawView{
    
    self.view.backgroundColor = UIColorFromRGB(0xfafafa);
    
    UILabel *name = [UILabel labelWithTextColor:kTextColor font:15 backgroundColor:nil Text:@""];
    UILabel *tel = [UILabel labelWithTextColor:kTextColor font:15 backgroundColor:nil Text:@""];
    UILabel *comitTime = [UILabel labelWithTextColor:kTextColor font:15 backgroundColor:nil Text:@""];
    UILabel *content = [UILabel labelWithTextColor:kTextColor font:15 backgroundColor:nil Text:@""];
    UILabel *endLabel = [UILabel labelWithTextColor:kTextColor font:15 backgroundColor:nil Text:@""];
    
    if ([self.type isEqualToString:@"请假通知"]) {
        name.text = @"姓名:";
        tel.text = @"会议名称:";
        comitTime.text = @"手机号:";
        content.text = @"提交时间:";
        endLabel.text = @"请假原因";
    }else if ([self.type isEqualToString:@"审核通知"]) {
        name.text = @"姓名:";
        tel.text = @"性别:";
        comitTime.text = @"手机号:";
        content.text = @"提交时间:";
        endLabel.text = @"发展步骤:";
    }
    [self.view sd_addSubviews:@[name, tel, comitTime, content, endLabel]];
    
    name.sd_layout
    .leftSpaceToView(self.view, 12)
    .topSpaceToView(self.view, 16)
    .heightIs(20)
    .widthIs(180 * kWScale);
    
    tel.sd_layout
    .leftSpaceToView(self.view, 12)
    .topSpaceToView(name, 16)
    .heightIs(20)
    .widthIs(180 * kWScale);
    
    comitTime.sd_layout
    .leftSpaceToView(self.view, 12)
    .topSpaceToView(tel, 16)
    .heightIs(20)
    .widthIs(180 * kWScale);
    
    content.sd_layout
    .leftSpaceToView(self.view, 12)
    .topSpaceToView(comitTime, 16)
    .heightIs(20)
    .widthIs(180 * kWScale);
    
    endLabel.sd_layout
    .leftSpaceToView(self.view, 12)
    .topSpaceToView(content, 16)
    .heightIs(20)
    .widthIs(180 * kWScale);
    
    
    self.firstLabel = [UILabel labelWithTextColor:kTextColor font:17 backgroundColor:nil Text:@""];
    self.secondLabel = [UILabel labelWithTextColor:kTextColor font:17 backgroundColor:nil Text:@""];
    self.thirdLabel = [UILabel labelWithTextColor:kTextColor font:17 backgroundColor:nil Text:@""];
    self.forthLabel = [UILabel labelWithTextColor:kTextColor font:17 backgroundColor:nil Text:@""];
    self.endLabel = [UILabel labelWithTextColor:kTextColor font:17 backgroundColor:nil Text:@""];
    self.endLabel.numberOfLines = 0;
    [self.view sd_addSubviews:@[self.firstLabel, self.secondLabel, self.thirdLabel, self.forthLabel, self.endLabel]];
    
    self.firstLabel.sd_layout
    .leftSpaceToView(self.view, 200 * kWScale)
    .centerYEqualToView(name)
    .heightIs(24)
    .rightSpaceToView(self.view, 12);
    
    self.secondLabel.sd_layout
    .leftSpaceToView(self.view, 200 * kWScale)
    .centerYEqualToView(tel)
    .heightIs(24)
    .rightSpaceToView(self.view, 12);
    
    self.thirdLabel.sd_layout
    .leftSpaceToView(self.view, 200 * kWScale)
    .centerYEqualToView(comitTime)
    .heightIs(24)
    .rightSpaceToView(self.view, 12);
    
    self.forthLabel.sd_layout
    .leftSpaceToView(self.view, 200 * kWScale)
    .centerYEqualToView(content)
    .rightSpaceToView(self.view, 12)
    .heightIs(24);
    
    self.endLabel.sd_layout
    .leftSpaceToView(self.view, 200 * kWScale)
    .centerYEqualToView(endLabel)
    .rightSpaceToView(self.view, 12)
    .autoHeightRatio(0);
    
    //底部按钮
    UIButton *lfBtn = [UIButton buttonWithTextColor:kTextColor font:17 backgroudColor:[UIColor whiteColor] text:@"拒绝"];
    self.btmFirBtn = lfBtn;
    self.btmFirBtn.hidden = YES;
    [lfBtn addTarget:self action:@selector(actionForLfBtn) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *rgBtn = [UIButton buttonWithTextColor:[UIColor whiteColor] font:17 backgroudColor:AppTintColor text:@"同意"];
    self.btmSecBtn = rgBtn;
    self.btmSecBtn.hidden = YES;
    [rgBtn addTarget:self action:@selector(actionForRgBtn) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view sd_addSubviews:@[lfBtn, rgBtn]];
    
    lfBtn.sd_layout
    .leftSpaceToView(self.view, 0)
    .widthIs(ScreenWidth * 0.5)
    .bottomSpaceToView(self.view, kBottomSafeAreaHeight)
    .heightIs(50);
    
    rgBtn.sd_layout
    .rightSpaceToView(self.view, 0)
    .widthIs(ScreenWidth * 0.5)
    .bottomSpaceToView(self.view, kBottomSafeAreaHeight)
    .heightIs(50);
    
    
    //水印
    self.agreeImageView = [UIImageView new];
    self.agreeImageView.hidden = YES;
    [self.view addSubview:self.agreeImageView];
    
    self.agreeImageView.sd_layout
    .widthIs(200 * kWScale)
    .heightIs(81 * kWScale)
    .bottomSpaceToView(self.view, kBottomSafeAreaHeight + 200)
    .rightSpaceToView(self.view, 40);
    
}


#pragma mark - action
//left(拒绝)
- (void)actionForLfBtn{
    if ([self.type isEqualToString:@"请假通知"]) {
        [DECommenAlert showAlertWithContent:@"确定拒绝该申请吗?" leftBtn:@"取消" rightBtn:@"确定" tapEvent:^(NSInteger index) {
            [DECommenAlert dismiss];
            if (index == 1) {
                [self requestForQingJiaAgree:0];
            }
        }];
    }else if ([self.type isEqualToString:@"审核通知"]) {
        [DECommenAlert showAlertWithContent:@"确定同意该申请吗?" leftBtn:@"取消" rightBtn:@"确定" tapEvent:^(NSInteger index) {
            [DECommenAlert dismiss];
            if (index == 1) {
                [self requestForApplyAgree:0];
            }
        }];
    }
}

//rg(同意)
- (void)actionForRgBtn{
    if ([self.type isEqualToString:@"请假通知"]) {
        [DECommenAlert showAlertWithContent:@"确定同意该申请吗?" leftBtn:@"取消" rightBtn:@"确定" tapEvent:^(NSInteger index) {
            [DECommenAlert dismiss];
            if (index == 1) {
                [self requestForQingJiaAgree:1];
            }
        }];
    }else if ([self.type isEqualToString:@"审核通知"]) {
        [DECommenAlert showAlertWithContent:@"确定同意该申请吗?" leftBtn:@"取消" rightBtn:@"确定" tapEvent:^(NSInteger index) {
            [DECommenAlert dismiss];
            if (index == 1) {
                [self requestForApplyAgree:1];
            }
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
