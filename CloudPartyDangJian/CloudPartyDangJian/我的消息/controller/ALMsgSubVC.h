//
//  ALMsgSubVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ALMsgSubVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
/** 根视图 */
@property (nonatomic, strong)UITableView *tableView;
//models
@property (nonatomic, strong)NSArray *modelArray;

@end

NS_ASSUME_NONNULL_END
