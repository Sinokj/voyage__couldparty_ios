//
//  MyMessageVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "MyMessageVC.h"
#import "ALMsgSubVC.h"
#import "ALMyMsgModel.h"

@interface MyMessageVC ()

@end

@implementation MyMessageVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //标题
    self.title = @"我的消息";
    
    [self setTabBarFrame:CGRectMake(0, 0, ScreenWidth, 44)
        contentViewFrame:CGRectMake(0, 44, ScreenWidth, ScreenHeight - kTopHeight  - 44 - kBottomSafeAreaHeight)];
    self.tabBar.itemTitleColor = [UIColor lightGrayColor];
    self.tabBar.itemTitleSelectedColor = AppTintColor;
    self.tabBar.itemTitleFont = [UIFont systemFontOfSize:17];
    self.tabBar.itemTitleSelectedFont = [UIFont boldSystemFontOfSize:17];
    
    self.tabBar.indicatorScrollFollowContent = YES;
    self.tabBar.indicatorColor = AppTintColor;
    
    [self.tabBar setIndicatorWidth:40 marginTop:40 marginBottom:0 tapSwitchAnimated:NO];
    
    [self.tabContentView setContentScrollEnabled:YES tapSwitchAnimated:NO];
    self.tabContentView.loadViewOfChildContollerWhileAppear = YES;
    [self initViewControllers];
    
}



- (void)initViewControllers{
    NSArray *tempArray = @[@"待处理", @"已处理"];
    NSMutableArray *controllers = [NSMutableArray array];
    for (int i = 0; i < tempArray.count; i ++) {
        NSString *name = tempArray[i];
        ALMsgSubVC *subVC = [ALMsgSubVC new];
        subVC.yp_tabItemTitle = name;
        [controllers addObject:subVC];
    }
    self.viewControllers = controllers;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
