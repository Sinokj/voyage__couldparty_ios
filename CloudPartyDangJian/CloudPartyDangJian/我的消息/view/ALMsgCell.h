//
//  ALMsgCell.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALMyMsgModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ALMsgCell : UITableViewCell
@property (nonatomic, strong)ALMyMsgModel *model;
@end

NS_ASSUME_NONNULL_END
