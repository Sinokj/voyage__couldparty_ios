//
//  AppDelegate.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/19.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabbarController.h"
// haha
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UIView *lanuchView;

@property (nonatomic, strong) UIImageView *lanuchImageView;
@property (nonatomic, strong) TabbarController *tabBarVc;

/***  是否允许横屏的标记 */
@property (nonatomic,assign)BOOL allowRotation;
@end

