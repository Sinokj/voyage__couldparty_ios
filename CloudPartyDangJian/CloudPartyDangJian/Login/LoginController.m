//
//  LoginController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "LoginController.h"
#import "ForgetPwController.h"
#import "JPUSHService.h"
#import "NSObject+rootVcExtension.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "WebViewController.h"
@interface LoginController ()
@property (weak, nonatomic) IBOutlet UIView *loginContentView;

@property (weak, nonatomic) IBOutlet UIImageView *partyIcon;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgotBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconWidthConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *middleWhiteViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *middleIconView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end

@implementation LoginController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.fd_interactivePopDisabled = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self setupTheMainView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)setupNaviLeftItem {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 2, 15, 20);
    [btn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(superBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
}

- (void)setupTheMainView {
    
    [self.closeBtn setBackgroundImage:[[UIImage imageNamed:@"login_close"] imageByTintColor:AppTintColor] forState:UIControlStateNormal];
    
    [STUtils setupView:self.loginContentView  cornerRadius:10 bgColor:[UIColor whiteColor] borderW:0 borderColor:nil];
    CGFloat bw = 8;
    self.partyIcon.layer.cornerRadius = MYDIMESCALEW(15);
    self.iconWidthConstant.constant = MYDIMESCALEW(80);
    self.iconTopConstraint.constant = MYDIMESCALEH(80);
//    self.partyIcon.layer.borderWidth = bw;
    // 为了解决同时设置图片边框和圆角背景色的白色虚线的问题 ？
    self.middleWhiteViewHeightConstraint.constant = MYDIMESCALEW(120) - 2 * bw;
    [STUtils setupView:self.middleIconView  cornerRadius:(MYDIMESCALEW(120) - 2 * bw)/2 bgColor:[UIColor whiteColor] borderW:0 borderColor:nil];
    self.partyIcon.layer.borderColor = self.view.backgroundColor.CGColor;
//    self.view.backgroundColor = [UIColor blackColor];
    self.partyIcon.clipsToBounds = YES;
//    self.partyIcon.backgroundColor = [UIColor yellowColor];
    
    self.userName.layer.cornerRadius = self.userName.frame.size.height/2;
    self.userName.layer.borderWidth = 1.0;
    self.userName.layer.borderColor = AppTintColor.CGColor;
    self.userName.delegate = self;
    [self setTextFieldLeftPadding:self.userName forWidth:20.0];
    
    self.password.layer.cornerRadius = self.password.frame.size.height/2;
    self.password.layer.borderWidth = 1.0;
    self.password.layer.borderColor = AppTintColor.CGColor;
    self.password.delegate = self;
    self.password.secureTextEntry = YES;
    [self setTextFieldLeftPadding:self.password forWidth:20.0];
    
    [self.loginBtn setTitle:@"登   录" forState:UIControlStateNormal];
    [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginBtn setBackgroundColor: AppTintColor];
    self.loginBtn.titleLabel.font = FontSystem(16);
    self.loginBtn.layer.cornerRadius = self.loginBtn.frame.size.height/2;
    [self.loginBtn addTarget:self action:@selector(loginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.forgotBtn addTarget:self action:@selector(forgetBtnClick) forControlEvents:UIControlEventTouchUpInside];
    //填写用户名和密码
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:@"username"] != nil) {
        self.userName.text = [user objectForKey:@"username"];
    }
    if ([user objectForKey:@"username"] != nil) {
        self.password.text = [user objectForKey:@"password"];
    }
    
    [self.userName becomeFirstResponder];
    
    /** 隐私协议 */
    [self.view layoutSubviews];
    UIButton *yinsiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [yinsiBtn addTarget:self action:@selector(actionForYinSiBtn) forControlEvents:UIControlEventTouchUpInside];
    [yinsiBtn setTitleColor:[UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1.00] forState:UIControlStateNormal];
    [yinsiBtn setTitle:@"用户隐私政策" forState:UIControlStateNormal];
    yinsiBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:yinsiBtn];

    yinsiBtn.frame = CGRectMake(0, ScreenHeight - 100, 200, 20);
    yinsiBtn.centerX = ScreenWidth  * 0.5;
    
}

#pragma mark - action
//隐私政策
- (void)actionForYinSiBtn{
    //   输入项目的隐私政策的 URL
    NSString *url = @"https://djy.ewanyuan.cn/cloudPartyApp/iosPrivate.html";
    WebViewController *w = [WebViewController new];
    w.type = @"1";
    w.tit = @"隐私政策";
    w.str = url;
    [self.navigationController pushViewController:w animated:YES];
}

- (IBAction)closePageClick:(id)sender {
    self.tabBarVc.selectedIndex = 0;
    [self.navigationController popViewControllerAnimated:YES];
}
//textField文字左对齐，并留有间隙
- (void)setTextFieldLeftPadding:(UITextField *)textField forWidth:(CGFloat)leftWidth {
    
    CGRect frame = textField.frame;
    frame.size.width = leftWidth;
    UIView *leftview = [[UIView alloc] initWithFrame:frame];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = leftview;
}

- (void)forgetBtnClick {
    
    ForgetPwController *forgetPw = [ForgetPwController new];
    [self.navigationController pushViewController:forgetPw animated:YES];
    
}

- (void)loginBtnClick:(UIButton *)sender {
    
    if ([self.userName.text isEqualToString:@""] || [self.password.text isEqualToString:@""]) {
        [MBProgressHUD showMessage:@"请输入用户名密码" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        return ;
    }
    
    NSData *dataTel = [self.userName.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64EncodeTel = [dataTel base64EncodedStringWithOptions:0];

    NSData *dataPaw = [self.password.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64EncodePaw = [dataPaw base64EncodedStringWithOptions:0];
    
    [DataRequest PostWithURL:[NSString stringWithFormat:@"%@?userId=%@&password=%@",Login,base64EncodeTel,base64EncodePaw] parameter:nil :^(id reponserObject) {
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            //登录成功存储cookie
            NSString *endUrl = [NSString stringWithFormat:@"%@/%@?userId=%@&password=%@",BaseUrl , Login, self.userName.text, self.password.text];
            [STUtils saveCookieWithUrl:endUrl];
            
            //存储信息
            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
            [user setObject:self.userName.text forKey:@"username"];
            [user setObject:self.password.text forKey:@"password"];
            [user setObject:reponserObject[@"partyGroupName"] forKey:@"partyGroupName"];
            [user setObject:reponserObject[@"vcName"] forKey:@"vcName"];
            
            if (reponserObject[@"nCommitteeId"]) {
                [STUtils setObject:reponserObject[@"nCommitteeId"] forKey:KDangJianType];
            }
            
            [STUtils setObject:@1 forKey:KIsLogin];

            //注册极光推送
//            NSString *registID = [JPUSHService registrationID];
            [DataRequest PostWithURL:BangDingPush parameter:@{@"vcPushId": [JPUSHService registrationID] == nil ? @"": [JPUSHService registrationID], @"vcPlatForm":@"IOS"} :^(id reponserObject) {
            }];

            //发送已经登录的通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginUpdateData" object:nil userInfo:nil];
            
            if ([self.password.text isEqualToString:@"123456"]) {
                UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"提示" message:@"登录成功,请及时修改密码" preferredStyle:(UIAlertControllerStyleAlert)];
                UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                [alertC addAction:alertA];
                [self presentViewController:alertC animated:YES completion:nil];
                
            }else {
                
                [MBProgressHUD showMessage:@"登陆成功" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            
            TabbarController *tabBarVc = self.tabBarVc;
            tabBarVc.selectedIndex = 0;
            
            WebViewController *w = self.webVc;
            [w.webV reload];

            //            BulletinController *bin = self.bullVc;
            //            [bin reloadData];
            
        }else {
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        }
    }];
}

- (void)updateData:(id)reponserObject{
    // 登录成功首页刷新数据
    TabbarController *tabBarVc = self.tabBarVc;
    tabBarVc.selectedIndex = 0;
    HomeController *homeVc = self.homeVc;
    [homeVc updateInfoAfterLoginIn:[reponserObject[@"nCommitteeId"] description]];
}

- (UITextField *)customWithFrame:(CGRect)frame andLeftTitle:(NSString *)leftT andPlaceH:(NSString *)placeho {
    
    UIImageView *label = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, frame.size.height, frame.size.height - 20)];
    label.contentMode = UIViewContentModeScaleAspectFit;
    label.image = [UIImage imageNamed:leftT];
    //    label.textColor = [UIColor colorWithRed:0.4353 green:0.4353 blue:0.4353 alpha:1.0];
    //    label.font = [UIFont systemFontOfSize:15];
    UITextField *textF = [[UITextField alloc] initWithFrame:frame];
    textF.leftViewMode = UITextFieldViewModeAlways;
    textF.layer.borderWidth = 1 / ScreenScale;
    textF.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1].CGColor;
    textF.leftView = label;
    textF.placeholder = placeho;
    //    textF.textAlignment = NSTextAlignmentCenter
    textF.clearButtonMode = UITextFieldViewModeWhileEditing;
    textF.font = [UIFont systemFontOfSize:13];
    textF.backgroundColor = [UIColor whiteColor];
    textF.borderStyle = UITextBorderStyleNone;
    return textF;
}

- (void)superBtnClick {
    if (!self.isTrue) {
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        UIWindow *aud = [UIApplication sharedApplication].keyWindow;
        UITabBarController *tab = (UITabBarController *)aud.rootViewController;
        tab.selectedIndex = 0;
    }
}
@end

